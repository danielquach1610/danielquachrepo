﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Lab12
{
    public partial class Form1 : Form
    {
        Timer aTimer = new Timer();
        int countDown = 100, count = 1;
        decimal heightDecrease = 0;
        private Point locationGas, initialLocation;

        public Form1()
        {
            InitializeComponent();
            heightDecrease = Math.Ceiling(decimal.Parse(pnlGas.Height.ToString()) / 100);
            locationGas = pnlGas.Location;
            initialLocation = pnlGas.Location;
            aTimer.Tick += new EventHandler(timer_Tick);
            aTimer.Interval = 100;
        }

        private void btnPour_Click(object sender, EventArgs e)
        {
            if(countDown <=0)
            {
                MessageBox.Show("Gas tank is empty","Warning",MessageBoxButtons.OK,MessageBoxIcon.Warning);
            }
            else
            {
                aTimer.Enabled = true;
                aTimer.Start();
            }  
        }

        void timer_Tick(object sender, EventArgs e)
        {
            pnlGas.Height = pnlGas.Height - int.Parse(heightDecrease.ToString());
            locationGas.Y += int.Parse(heightDecrease.ToString());
            pnlGas.Location = locationGas;
            countDown -= count;
            lblCount.Text = countDown.ToString();
            if (pnlGas.Height <= 0)
                aTimer.Stop();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            aTimer.Enabled = false;
            aTimer.Dispose();
            aTimer.Stop();
        }

        private void btnFill_Click(object sender, EventArgs e)
        {
            pnlGas.Height = 200;
            pnlGas.Location = initialLocation;
            locationGas = initialLocation;
            countDown = 100;
            lblCount.Text = countDown.ToString();
            heightDecrease = Math.Ceiling(decimal.Parse(pnlGas.Height.ToString()) / 100);
            aTimer.Dispose();
            aTimer.Stop();
        }
    }
}
