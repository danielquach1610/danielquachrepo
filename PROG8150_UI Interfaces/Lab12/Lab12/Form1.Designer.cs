﻿namespace Lab12
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.btnPour = new System.Windows.Forms.Button();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.lblCount = new System.Windows.Forms.Label();
            this.pnlTank = new System.Windows.Forms.Panel();
            this.pnlGas = new System.Windows.Forms.Panel();
            this.btnFill = new System.Windows.Forms.Button();
            this.btnStop = new System.Windows.Forms.Button();
            this.pnlTank.SuspendLayout();
            this.SuspendLayout();
            // 
            // btnPour
            // 
            this.btnPour.Location = new System.Drawing.Point(93, 125);
            this.btnPour.Name = "btnPour";
            this.btnPour.Size = new System.Drawing.Size(140, 72);
            this.btnPour.TabIndex = 1;
            this.btnPour.Text = "Bleed";
            this.btnPour.UseVisualStyleBackColor = true;
            this.btnPour.Click += new System.EventHandler(this.btnPour_Click);
            // 
            // lblCount
            // 
            this.lblCount.AutoSize = true;
            this.lblCount.Font = new System.Drawing.Font("Microsoft Sans Serif", 40F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCount.Location = new System.Drawing.Point(89, 33);
            this.lblCount.Name = "lblCount";
            this.lblCount.Size = new System.Drawing.Size(144, 76);
            this.lblCount.TabIndex = 2;
            this.lblCount.Text = "100";
            this.lblCount.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // pnlTank
            // 
            this.pnlTank.BackgroundImage = global::Lab12.Properties.Resources.GasTank_640;
            this.pnlTank.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pnlTank.Controls.Add(this.pnlGas);
            this.pnlTank.Location = new System.Drawing.Point(251, 33);
            this.pnlTank.Margin = new System.Windows.Forms.Padding(0);
            this.pnlTank.Name = "pnlTank";
            this.pnlTank.Size = new System.Drawing.Size(316, 432);
            this.pnlTank.TabIndex = 0;
            // 
            // pnlGas
            // 
            this.pnlGas.BackColor = System.Drawing.Color.CadetBlue;
            this.pnlGas.Location = new System.Drawing.Point(136, 171);
            this.pnlGas.Margin = new System.Windows.Forms.Padding(0);
            this.pnlGas.Name = "pnlGas";
            this.pnlGas.Size = new System.Drawing.Size(50, 200);
            this.pnlGas.TabIndex = 1;
            // 
            // btnFill
            // 
            this.btnFill.Location = new System.Drawing.Point(93, 350);
            this.btnFill.Name = "btnFill";
            this.btnFill.Size = new System.Drawing.Size(140, 61);
            this.btnFill.TabIndex = 3;
            this.btnFill.Text = "Fill";
            this.btnFill.UseVisualStyleBackColor = true;
            this.btnFill.Click += new System.EventHandler(this.btnFill_Click);
            // 
            // btnStop
            // 
            this.btnStop.Location = new System.Drawing.Point(93, 237);
            this.btnStop.Name = "btnStop";
            this.btnStop.Size = new System.Drawing.Size(140, 72);
            this.btnStop.TabIndex = 4;
            this.btnStop.Text = "Stop";
            this.btnStop.UseVisualStyleBackColor = true;
            this.btnStop.Click += new System.EventHandler(this.button1_Click);
            // 
            // Form1
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.ClientSize = new System.Drawing.Size(592, 496);
            this.Controls.Add(this.btnStop);
            this.Controls.Add(this.btnFill);
            this.Controls.Add(this.lblCount);
            this.Controls.Add(this.btnPour);
            this.Controls.Add(this.pnlTank);
            this.Name = "Form1";
            this.Text = "Form1";
            this.pnlTank.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Panel pnlTank;
        private System.Windows.Forms.Panel pnlGas;
        private System.Windows.Forms.Button btnPour;
        private System.Windows.Forms.Timer timer1;
        private System.Windows.Forms.Label lblCount;
        private System.Windows.Forms.Button btnFill;
        private System.Windows.Forms.Button btnStop;
    }
}

