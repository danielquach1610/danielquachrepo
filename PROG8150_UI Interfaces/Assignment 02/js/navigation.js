﻿function openNav() {
    document.getElementById("mySidenav").style.width = "200px";
    document.getElementById("main").style.marginLeft = "200px";
}

function closeNav() {
	if (window.matchMedia('(max-width: 1023px)').matches) {
	    document.getElementById("mySidenav").style.width = "0";
		document.getElementById("main").style.marginLeft = "0";	
	}
}

function toggleNav() {
    if (window.matchMedia('(max-width: 1023px)').matches) {
        if (document.getElementById("mySidenav").style.width == "200px") {
            closeNav();
        }
        else {
            openNav();
        }
    }
}