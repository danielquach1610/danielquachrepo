﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace PROG8150_Assignment01
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void calcButton_Click(object sender, EventArgs e)
        {
            System.Diagnostics.Process.Start(@"C:\Windows\System32\calc.exe");
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }

        private void cmdButton_Click(object sender, EventArgs e)
        {
            System.Diagnostics.Process.Start(@"C:\Windows\System32\cmd.exe");
        }

        private void helpToolStripMenuItem_Click(object sender, EventArgs e)
        {
        }

        private void notepadButton_Click(object sender, EventArgs e)
        {
            System.Diagnostics.Process.Start(@"C:\Windows\System32\notepad.exe");
        }

        private void calculatorToolStripMenuItem_Click(object sender, EventArgs e)
        {
            System.Diagnostics.Process.Start(@"C:\Windows\System32\calc.exe");
        }

        private void commandLineCMDToolStripMenuItem_Click(object sender, EventArgs e)
        {
            System.Diagnostics.Process.Start(@"C:\Windows\System32\cmd.exe");
        }

        private void notepadToolStripMenuItem_Click(object sender, EventArgs e)
        {
            System.Diagnostics.Process.Start(@"C:\Windows\System32\notepad.exe");
        }

        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void fileToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void largeToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Size = new Size(1250, 500);
            this.CenterToScreen();
            int largeSize = 350, X = 50, Y = 75;
            calcButton.Size = new Size(largeSize, largeSize);
            calcButton.Location = new Point(X, Y);
            cmdButton.Size = new Size(largeSize, largeSize);
            cmdButton.Location = new Point(X * 2 + largeSize, Y);
            notepadButton.Size = new Size(largeSize, largeSize);
            notepadButton.Location = new Point(X * 3 + largeSize*2, Y);
            largeToolStripMenuItem.Checked = true;
            smallToolStripMenuItem.Checked = false;
        }

        private void smallToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Size = new Size(1100, 450);
            this.CenterToScreen();
            int smallSize = 325, X = 25, Y = 50;
            calcButton.Size = new Size(smallSize, smallSize);
            calcButton.Location = new Point(X, Y);
            cmdButton.Size = new Size(smallSize, smallSize);
            cmdButton.Location = new Point(X * 2 + smallSize, Y);
            notepadButton.Size = new Size(smallSize, smallSize);
            notepadButton.Location = new Point(X * 3 + smallSize * 2, Y);
            smallToolStripMenuItem.Checked = true;
            largeToolStripMenuItem.Checked = false;
        }

        private void toolTip1_Popup(object sender, PopupEventArgs e)
        {
        }

        private void aboutApplicationToolStripMenuItem_Click(object sender, EventArgs e)
        {
            MessageBox.Show("This application will open Calculator, Command Line or Notepad application when clicking on accordingly button.", "Help", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }
    }
}
