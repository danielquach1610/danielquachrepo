﻿namespace PROG8150_Assignment03_SCADA
{
    partial class frmMain
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmMain));
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.rtbInfor = new System.Windows.Forms.RichTextBox();
            this.timerBlink = new System.Windows.Forms.Timer(this.components);
            this.timerAlarm = new System.Windows.Forms.Timer(this.components);
            this.btnShutDown = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.btnReset1 = new System.Windows.Forms.Button();
            this.btnReset2 = new System.Windows.Forms.Button();
            this.btnReset3 = new System.Windows.Forms.Button();
            this.btnReset4 = new System.Windows.Forms.Button();
            this.btnReset5 = new System.Windows.Forms.Button();
            this.btnReset6 = new System.Windows.Forms.Button();
            this.btnReset7 = new System.Windows.Forms.Button();
            this.btnReset8 = new System.Windows.Forms.Button();
            this.mnMain = new System.Windows.Forms.MenuStrip();
            this.oxygenTankMapToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.manualGuideToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.txtMin = new System.Windows.Forms.TextBox();
            this.txtMax = new System.Windows.Forms.TextBox();
            this.btnApply = new System.Windows.Forms.Button();
            this.pictureBox9 = new System.Windows.Forms.PictureBox();
            this.pictureBox8 = new System.Windows.Forms.PictureBox();
            this.pictureBox7 = new System.Windows.Forms.PictureBox();
            this.pictureBox6 = new System.Windows.Forms.PictureBox();
            this.pictureBox5 = new System.Windows.Forms.PictureBox();
            this.pictureBox4 = new System.Windows.Forms.PictureBox();
            this.pictureBox3 = new System.Windows.Forms.PictureBox();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.btnValve8 = new System.Windows.Forms.Button();
            this.pnlMeter8 = new System.Windows.Forms.Panel();
            this.pnlTank8 = new System.Windows.Forms.Panel();
            this.lblNo8 = new System.Windows.Forms.Label();
            this.lblTank8 = new System.Windows.Forms.Label();
            this.pnlP8 = new System.Windows.Forms.Panel();
            this.pnlPressure8 = new System.Windows.Forms.Panel();
            this.btnValve7 = new System.Windows.Forms.Button();
            this.pnlMeter7 = new System.Windows.Forms.Panel();
            this.pnlTank7 = new System.Windows.Forms.Panel();
            this.lblNo7 = new System.Windows.Forms.Label();
            this.lblTank7 = new System.Windows.Forms.Label();
            this.pnlP7 = new System.Windows.Forms.Panel();
            this.pnlPressure7 = new System.Windows.Forms.Panel();
            this.btnValve6 = new System.Windows.Forms.Button();
            this.pnlMeter6 = new System.Windows.Forms.Panel();
            this.pnlTank6 = new System.Windows.Forms.Panel();
            this.lblNo6 = new System.Windows.Forms.Label();
            this.lblTank6 = new System.Windows.Forms.Label();
            this.pnlP6 = new System.Windows.Forms.Panel();
            this.pnlPressure6 = new System.Windows.Forms.Panel();
            this.btnValve5 = new System.Windows.Forms.Button();
            this.pnlMeter5 = new System.Windows.Forms.Panel();
            this.pnlTank5 = new System.Windows.Forms.Panel();
            this.lblNo5 = new System.Windows.Forms.Label();
            this.lblTank5 = new System.Windows.Forms.Label();
            this.pnlP5 = new System.Windows.Forms.Panel();
            this.pnlPressure5 = new System.Windows.Forms.Panel();
            this.btnValve4 = new System.Windows.Forms.Button();
            this.pnlMeter4 = new System.Windows.Forms.Panel();
            this.pnlTank4 = new System.Windows.Forms.Panel();
            this.lblNo4 = new System.Windows.Forms.Label();
            this.lblTank4 = new System.Windows.Forms.Label();
            this.pnlP4 = new System.Windows.Forms.Panel();
            this.pnlPressure4 = new System.Windows.Forms.Panel();
            this.btnValve3 = new System.Windows.Forms.Button();
            this.pnlMeter3 = new System.Windows.Forms.Panel();
            this.pnlTank3 = new System.Windows.Forms.Panel();
            this.lblNo3 = new System.Windows.Forms.Label();
            this.lblTank3 = new System.Windows.Forms.Label();
            this.pnlP3 = new System.Windows.Forms.Panel();
            this.pnlPressure3 = new System.Windows.Forms.Panel();
            this.btnValve2 = new System.Windows.Forms.Button();
            this.pnlMeter2 = new System.Windows.Forms.Panel();
            this.btnValve1 = new System.Windows.Forms.Button();
            this.pnlMeter1 = new System.Windows.Forms.Panel();
            this.pnlTank1 = new System.Windows.Forms.Panel();
            this.lblNo1 = new System.Windows.Forms.Label();
            this.lblTank1 = new System.Windows.Forms.Label();
            this.pnlP1 = new System.Windows.Forms.Panel();
            this.pnlPressure1 = new System.Windows.Forms.Panel();
            this.pnlTank2 = new System.Windows.Forms.Panel();
            this.lblNo2 = new System.Windows.Forms.Label();
            this.lblTank2 = new System.Windows.Forms.Label();
            this.pnlP2 = new System.Windows.Forms.Panel();
            this.pnlPressure2 = new System.Windows.Forms.Panel();
            this.mnMain.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.pnlTank8.SuspendLayout();
            this.pnlP8.SuspendLayout();
            this.pnlTank7.SuspendLayout();
            this.pnlP7.SuspendLayout();
            this.pnlTank6.SuspendLayout();
            this.pnlP6.SuspendLayout();
            this.pnlTank5.SuspendLayout();
            this.pnlP5.SuspendLayout();
            this.pnlTank4.SuspendLayout();
            this.pnlP4.SuspendLayout();
            this.pnlTank3.SuspendLayout();
            this.pnlP3.SuspendLayout();
            this.pnlTank1.SuspendLayout();
            this.pnlP1.SuspendLayout();
            this.pnlTank2.SuspendLayout();
            this.pnlP2.SuspendLayout();
            this.SuspendLayout();
            // 
            // timer1
            // 
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // rtbInfor
            // 
            this.rtbInfor.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rtbInfor.Location = new System.Drawing.Point(65, 516);
            this.rtbInfor.Name = "rtbInfor";
            this.rtbInfor.ReadOnly = true;
            this.rtbInfor.Size = new System.Drawing.Size(739, 288);
            this.rtbInfor.TabIndex = 15;
            this.rtbInfor.Text = "SAMPLE TEXT";
            // 
            // timerBlink
            // 
            this.timerBlink.Tick += new System.EventHandler(this.timerBlink_Tick);
            // 
            // timerAlarm
            // 
            this.timerAlarm.Tick += new System.EventHandler(this.timerAlarm_Tick);
            // 
            // btnShutDown
            // 
            this.btnShutDown.FlatAppearance.BorderColor = System.Drawing.Color.Red;
            this.btnShutDown.FlatAppearance.BorderSize = 5;
            this.btnShutDown.Font = new System.Drawing.Font("Microsoft Sans Serif", 20F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnShutDown.Location = new System.Drawing.Point(482, 432);
            this.btnShutDown.Name = "btnShutDown";
            this.btnShutDown.Size = new System.Drawing.Size(322, 67);
            this.btnShutDown.TabIndex = 25;
            this.btnShutDown.Text = "SHUT DOWN";
            this.btnShutDown.UseVisualStyleBackColor = true;
            this.btnShutDown.Click += new System.EventHandler(this.btnShutDown_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.Gold;
            this.label1.Location = new System.Drawing.Point(63, 428);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(142, 36);
            this.label1.TabIndex = 26;
            this.label1.Text = "Minimum:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.Red;
            this.label2.Location = new System.Drawing.Point(63, 470);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(149, 36);
            this.label2.TabIndex = 27;
            this.label2.Text = "Maximum:";
            // 
            // btnReset1
            // 
            this.btnReset1.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnReset1.Location = new System.Drawing.Point(65, 348);
            this.btnReset1.Name = "btnReset1";
            this.btnReset1.Size = new System.Drawing.Size(72, 50);
            this.btnReset1.TabIndex = 28;
            this.btnReset1.Text = "OK";
            this.btnReset1.UseVisualStyleBackColor = true;
            this.btnReset1.Click += new System.EventHandler(this.btnReset1_Click);
            // 
            // btnReset2
            // 
            this.btnReset2.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnReset2.Location = new System.Drawing.Point(257, 348);
            this.btnReset2.Name = "btnReset2";
            this.btnReset2.Size = new System.Drawing.Size(72, 50);
            this.btnReset2.TabIndex = 29;
            this.btnReset2.Text = "OK";
            this.btnReset2.UseVisualStyleBackColor = true;
            this.btnReset2.Click += new System.EventHandler(this.btnReset2_Click);
            // 
            // btnReset3
            // 
            this.btnReset3.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnReset3.Location = new System.Drawing.Point(455, 348);
            this.btnReset3.Name = "btnReset3";
            this.btnReset3.Size = new System.Drawing.Size(72, 50);
            this.btnReset3.TabIndex = 30;
            this.btnReset3.Text = "OK";
            this.btnReset3.UseVisualStyleBackColor = true;
            this.btnReset3.Click += new System.EventHandler(this.btnReset3_Click);
            // 
            // btnReset4
            // 
            this.btnReset4.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnReset4.Location = new System.Drawing.Point(651, 348);
            this.btnReset4.Name = "btnReset4";
            this.btnReset4.Size = new System.Drawing.Size(72, 50);
            this.btnReset4.TabIndex = 31;
            this.btnReset4.Text = "OK";
            this.btnReset4.UseVisualStyleBackColor = true;
            this.btnReset4.Click += new System.EventHandler(this.btnReset4_Click);
            // 
            // btnReset5
            // 
            this.btnReset5.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnReset5.Location = new System.Drawing.Point(852, 348);
            this.btnReset5.Name = "btnReset5";
            this.btnReset5.Size = new System.Drawing.Size(72, 50);
            this.btnReset5.TabIndex = 32;
            this.btnReset5.Text = "OK";
            this.btnReset5.UseVisualStyleBackColor = true;
            this.btnReset5.Click += new System.EventHandler(this.btnReset5_Click);
            // 
            // btnReset6
            // 
            this.btnReset6.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnReset6.Location = new System.Drawing.Point(1052, 348);
            this.btnReset6.Name = "btnReset6";
            this.btnReset6.Size = new System.Drawing.Size(72, 50);
            this.btnReset6.TabIndex = 33;
            this.btnReset6.Text = "OK";
            this.btnReset6.UseVisualStyleBackColor = true;
            this.btnReset6.Click += new System.EventHandler(this.btnReset6_Click);
            // 
            // btnReset7
            // 
            this.btnReset7.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnReset7.Location = new System.Drawing.Point(852, 748);
            this.btnReset7.Name = "btnReset7";
            this.btnReset7.Size = new System.Drawing.Size(72, 50);
            this.btnReset7.TabIndex = 34;
            this.btnReset7.Text = "OK";
            this.btnReset7.UseVisualStyleBackColor = true;
            this.btnReset7.Click += new System.EventHandler(this.btnReset7_Click);
            // 
            // btnReset8
            // 
            this.btnReset8.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnReset8.Location = new System.Drawing.Point(1052, 748);
            this.btnReset8.Name = "btnReset8";
            this.btnReset8.Size = new System.Drawing.Size(72, 50);
            this.btnReset8.TabIndex = 35;
            this.btnReset8.Text = "OK";
            this.btnReset8.UseVisualStyleBackColor = true;
            this.btnReset8.Click += new System.EventHandler(this.btnReset8_Click);
            // 
            // mnMain
            // 
            this.mnMain.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.mnMain.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.oxygenTankMapToolStripMenuItem,
            this.manualGuideToolStripMenuItem});
            this.mnMain.Location = new System.Drawing.Point(0, 0);
            this.mnMain.Name = "mnMain";
            this.mnMain.Size = new System.Drawing.Size(1276, 28);
            this.mnMain.TabIndex = 36;
            this.mnMain.Text = "menuStrip1";
            // 
            // oxygenTankMapToolStripMenuItem
            // 
            this.oxygenTankMapToolStripMenuItem.Name = "oxygenTankMapToolStripMenuItem";
            this.oxygenTankMapToolStripMenuItem.Size = new System.Drawing.Size(138, 24);
            this.oxygenTankMapToolStripMenuItem.Text = "Oxygen Tank Map";
            this.oxygenTankMapToolStripMenuItem.Click += new System.EventHandler(this.oxygenTankMapToolStripMenuItem_Click);
            // 
            // manualGuideToolStripMenuItem
            // 
            this.manualGuideToolStripMenuItem.Name = "manualGuideToolStripMenuItem";
            this.manualGuideToolStripMenuItem.Size = new System.Drawing.Size(93, 24);
            this.manualGuideToolStripMenuItem.Text = "User Guide";
            this.manualGuideToolStripMenuItem.Click += new System.EventHandler(this.manualGuideToolStripMenuItem_Click);
            // 
            // txtMin
            // 
            this.txtMin.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtMin.Location = new System.Drawing.Point(216, 423);
            this.txtMin.Name = "txtMin";
            this.txtMin.Size = new System.Drawing.Size(86, 41);
            this.txtMin.TabIndex = 37;
            this.txtMin.Text = "20";
            this.txtMin.TextChanged += new System.EventHandler(this.txtMin_TextChanged);
            // 
            // txtMax
            // 
            this.txtMax.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtMax.Location = new System.Drawing.Point(216, 467);
            this.txtMax.Name = "txtMax";
            this.txtMax.Size = new System.Drawing.Size(86, 41);
            this.txtMax.TabIndex = 38;
            this.txtMax.Text = "80";
            this.txtMax.TextChanged += new System.EventHandler(this.txtMax_TextChanged);
            // 
            // btnApply
            // 
            this.btnApply.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnApply.Location = new System.Drawing.Point(308, 432);
            this.btnApply.Name = "btnApply";
            this.btnApply.Size = new System.Drawing.Size(126, 67);
            this.btnApply.TabIndex = 39;
            this.btnApply.Text = "APPLY";
            this.btnApply.UseVisualStyleBackColor = true;
            this.btnApply.Click += new System.EventHandler(this.btnApply_Click);
            // 
            // pictureBox9
            // 
            this.pictureBox9.BackColor = System.Drawing.Color.Transparent;
            this.pictureBox9.BackgroundImage = global::PROG8150_Assignment03_SCADA.Properties.Resources.vertical;
            this.pictureBox9.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureBox9.Location = new System.Drawing.Point(1213, 184);
            this.pictureBox9.Name = "pictureBox9";
            this.pictureBox9.Size = new System.Drawing.Size(25, 334);
            this.pictureBox9.TabIndex = 48;
            this.pictureBox9.TabStop = false;
            // 
            // pictureBox8
            // 
            this.pictureBox8.BackColor = System.Drawing.Color.Transparent;
            this.pictureBox8.BackgroundImage = global::PROG8150_Assignment03_SCADA.Properties.Resources.topTransperent;
            this.pictureBox8.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.pictureBox8.Location = new System.Drawing.Point(1187, 134);
            this.pictureBox8.Name = "pictureBox8";
            this.pictureBox8.Size = new System.Drawing.Size(52, 51);
            this.pictureBox8.TabIndex = 47;
            this.pictureBox8.TabStop = false;
            // 
            // pictureBox7
            // 
            this.pictureBox7.BackColor = System.Drawing.Color.Transparent;
            this.pictureBox7.BackgroundImage = global::PROG8150_Assignment03_SCADA.Properties.Resources.bottomTransperent;
            this.pictureBox7.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.pictureBox7.Location = new System.Drawing.Point(1187, 516);
            this.pictureBox7.Name = "pictureBox7";
            this.pictureBox7.Size = new System.Drawing.Size(52, 51);
            this.pictureBox7.TabIndex = 46;
            this.pictureBox7.TabStop = false;
            // 
            // pictureBox6
            // 
            this.pictureBox6.BackColor = System.Drawing.SystemColors.Control;
            this.pictureBox6.BackgroundImage = global::PROG8150_Assignment03_SCADA.Properties.Resources.straightDuct;
            this.pictureBox6.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.pictureBox6.Location = new System.Drawing.Point(986, 537);
            this.pictureBox6.Name = "pictureBox6";
            this.pictureBox6.Size = new System.Drawing.Size(86, 27);
            this.pictureBox6.TabIndex = 45;
            this.pictureBox6.TabStop = false;
            // 
            // pictureBox5
            // 
            this.pictureBox5.BackColor = System.Drawing.SystemColors.Control;
            this.pictureBox5.BackgroundImage = global::PROG8150_Assignment03_SCADA.Properties.Resources.straightDuct;
            this.pictureBox5.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.pictureBox5.Location = new System.Drawing.Point(986, 134);
            this.pictureBox5.Name = "pictureBox5";
            this.pictureBox5.Size = new System.Drawing.Size(86, 27);
            this.pictureBox5.TabIndex = 44;
            this.pictureBox5.TabStop = false;
            // 
            // pictureBox4
            // 
            this.pictureBox4.BackColor = System.Drawing.SystemColors.Control;
            this.pictureBox4.BackgroundImage = global::PROG8150_Assignment03_SCADA.Properties.Resources.straightDuct;
            this.pictureBox4.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.pictureBox4.Location = new System.Drawing.Point(785, 134);
            this.pictureBox4.Name = "pictureBox4";
            this.pictureBox4.Size = new System.Drawing.Size(86, 27);
            this.pictureBox4.TabIndex = 43;
            this.pictureBox4.TabStop = false;
            // 
            // pictureBox3
            // 
            this.pictureBox3.BackColor = System.Drawing.SystemColors.Control;
            this.pictureBox3.BackgroundImage = global::PROG8150_Assignment03_SCADA.Properties.Resources.straightDuct;
            this.pictureBox3.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.pictureBox3.Location = new System.Drawing.Point(586, 134);
            this.pictureBox3.Name = "pictureBox3";
            this.pictureBox3.Size = new System.Drawing.Size(86, 27);
            this.pictureBox3.TabIndex = 42;
            this.pictureBox3.TabStop = false;
            // 
            // pictureBox2
            // 
            this.pictureBox2.BackColor = System.Drawing.SystemColors.Control;
            this.pictureBox2.BackgroundImage = global::PROG8150_Assignment03_SCADA.Properties.Resources.straightDuct;
            this.pictureBox2.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.pictureBox2.Location = new System.Drawing.Point(388, 134);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(88, 27);
            this.pictureBox2.TabIndex = 41;
            this.pictureBox2.TabStop = false;
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackColor = System.Drawing.Color.Transparent;
            this.pictureBox1.BackgroundImage = global::PROG8150_Assignment03_SCADA.Properties.Resources.straightDuct;
            this.pictureBox1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.pictureBox1.Location = new System.Drawing.Point(196, 134);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(81, 27);
            this.pictureBox1.TabIndex = 40;
            this.pictureBox1.TabStop = false;
            // 
            // btnValve8
            // 
            this.btnValve8.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnValve8.BackgroundImage")));
            this.btnValve8.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.btnValve8.Location = new System.Drawing.Point(1135, 748);
            this.btnValve8.Name = "btnValve8";
            this.btnValve8.Size = new System.Drawing.Size(70, 50);
            this.btnValve8.TabIndex = 24;
            this.btnValve8.UseVisualStyleBackColor = true;
            this.btnValve8.Click += new System.EventHandler(this.btnValve8_Click);
            // 
            // pnlMeter8
            // 
            this.pnlMeter8.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pnlMeter8.BackgroundImage")));
            this.pnlMeter8.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.pnlMeter8.Location = new System.Drawing.Point(1052, 668);
            this.pnlMeter8.Name = "pnlMeter8";
            this.pnlMeter8.Size = new System.Drawing.Size(153, 74);
            this.pnlMeter8.TabIndex = 23;
            // 
            // pnlTank8
            // 
            this.pnlTank8.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pnlTank8.BackgroundImage")));
            this.pnlTank8.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.pnlTank8.Controls.Add(this.lblNo8);
            this.pnlTank8.Controls.Add(this.lblTank8);
            this.pnlTank8.Controls.Add(this.pnlP8);
            this.pnlTank8.Location = new System.Drawing.Point(1052, 443);
            this.pnlTank8.Name = "pnlTank8";
            this.pnlTank8.Size = new System.Drawing.Size(153, 219);
            this.pnlTank8.TabIndex = 22;
            // 
            // lblNo8
            // 
            this.lblNo8.AutoSize = true;
            this.lblNo8.BackColor = System.Drawing.Color.Transparent;
            this.lblNo8.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblNo8.Location = new System.Drawing.Point(64, 4);
            this.lblNo8.Name = "lblNo8";
            this.lblNo8.Size = new System.Drawing.Size(32, 36);
            this.lblNo8.TabIndex = 26;
            this.lblNo8.Text = "8";
            // 
            // lblTank8
            // 
            this.lblTank8.AutoSize = true;
            this.lblTank8.BackColor = System.Drawing.Color.Transparent;
            this.lblTank8.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTank8.Location = new System.Drawing.Point(48, 164);
            this.lblTank8.Name = "lblTank8";
            this.lblTank8.Size = new System.Drawing.Size(55, 29);
            this.lblTank8.TabIndex = 7;
            this.lblTank8.Text = "100";
            // 
            // pnlP8
            // 
            this.pnlP8.BackColor = System.Drawing.Color.Transparent;
            this.pnlP8.Controls.Add(this.pnlPressure8);
            this.pnlP8.Location = new System.Drawing.Point(53, 61);
            this.pnlP8.Name = "pnlP8";
            this.pnlP8.Size = new System.Drawing.Size(50, 100);
            this.pnlP8.TabIndex = 7;
            // 
            // pnlPressure8
            // 
            this.pnlPressure8.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.pnlPressure8.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnlPressure8.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlPressure8.Location = new System.Drawing.Point(0, 0);
            this.pnlPressure8.Name = "pnlPressure8";
            this.pnlPressure8.Size = new System.Drawing.Size(50, 100);
            this.pnlPressure8.TabIndex = 1;
            // 
            // btnValve7
            // 
            this.btnValve7.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnValve7.BackgroundImage")));
            this.btnValve7.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.btnValve7.Location = new System.Drawing.Point(935, 748);
            this.btnValve7.Name = "btnValve7";
            this.btnValve7.Size = new System.Drawing.Size(70, 50);
            this.btnValve7.TabIndex = 21;
            this.btnValve7.UseVisualStyleBackColor = true;
            this.btnValve7.Click += new System.EventHandler(this.btnValve7_Click);
            // 
            // pnlMeter7
            // 
            this.pnlMeter7.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pnlMeter7.BackgroundImage")));
            this.pnlMeter7.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.pnlMeter7.Location = new System.Drawing.Point(852, 668);
            this.pnlMeter7.Name = "pnlMeter7";
            this.pnlMeter7.Size = new System.Drawing.Size(153, 74);
            this.pnlMeter7.TabIndex = 20;
            // 
            // pnlTank7
            // 
            this.pnlTank7.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pnlTank7.BackgroundImage")));
            this.pnlTank7.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.pnlTank7.Controls.Add(this.lblNo7);
            this.pnlTank7.Controls.Add(this.lblTank7);
            this.pnlTank7.Controls.Add(this.pnlP7);
            this.pnlTank7.Location = new System.Drawing.Point(852, 443);
            this.pnlTank7.Name = "pnlTank7";
            this.pnlTank7.Size = new System.Drawing.Size(153, 219);
            this.pnlTank7.TabIndex = 19;
            // 
            // lblNo7
            // 
            this.lblNo7.AutoSize = true;
            this.lblNo7.BackColor = System.Drawing.Color.Transparent;
            this.lblNo7.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblNo7.Location = new System.Drawing.Point(63, 4);
            this.lblNo7.Name = "lblNo7";
            this.lblNo7.Size = new System.Drawing.Size(32, 36);
            this.lblNo7.TabIndex = 26;
            this.lblNo7.Text = "7";
            // 
            // lblTank7
            // 
            this.lblTank7.AutoSize = true;
            this.lblTank7.BackColor = System.Drawing.Color.Transparent;
            this.lblTank7.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTank7.Location = new System.Drawing.Point(50, 164);
            this.lblTank7.Name = "lblTank7";
            this.lblTank7.Size = new System.Drawing.Size(55, 29);
            this.lblTank7.TabIndex = 7;
            this.lblTank7.Text = "100";
            // 
            // pnlP7
            // 
            this.pnlP7.BackColor = System.Drawing.Color.Transparent;
            this.pnlP7.Controls.Add(this.pnlPressure7);
            this.pnlP7.Location = new System.Drawing.Point(55, 61);
            this.pnlP7.Name = "pnlP7";
            this.pnlP7.Size = new System.Drawing.Size(50, 100);
            this.pnlP7.TabIndex = 7;
            // 
            // pnlPressure7
            // 
            this.pnlPressure7.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.pnlPressure7.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnlPressure7.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlPressure7.Location = new System.Drawing.Point(0, 0);
            this.pnlPressure7.Name = "pnlPressure7";
            this.pnlPressure7.Size = new System.Drawing.Size(50, 100);
            this.pnlPressure7.TabIndex = 1;
            // 
            // btnValve6
            // 
            this.btnValve6.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnValve6.BackgroundImage")));
            this.btnValve6.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.btnValve6.Location = new System.Drawing.Point(1135, 348);
            this.btnValve6.Name = "btnValve6";
            this.btnValve6.Size = new System.Drawing.Size(70, 50);
            this.btnValve6.TabIndex = 18;
            this.btnValve6.UseVisualStyleBackColor = true;
            this.btnValve6.Click += new System.EventHandler(this.btnValve6_Click);
            // 
            // pnlMeter6
            // 
            this.pnlMeter6.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pnlMeter6.BackgroundImage")));
            this.pnlMeter6.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.pnlMeter6.Location = new System.Drawing.Point(1052, 268);
            this.pnlMeter6.Name = "pnlMeter6";
            this.pnlMeter6.Size = new System.Drawing.Size(153, 74);
            this.pnlMeter6.TabIndex = 17;
            // 
            // pnlTank6
            // 
            this.pnlTank6.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pnlTank6.BackgroundImage")));
            this.pnlTank6.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.pnlTank6.Controls.Add(this.lblNo6);
            this.pnlTank6.Controls.Add(this.lblTank6);
            this.pnlTank6.Controls.Add(this.pnlP6);
            this.pnlTank6.Location = new System.Drawing.Point(1052, 43);
            this.pnlTank6.Name = "pnlTank6";
            this.pnlTank6.Size = new System.Drawing.Size(153, 219);
            this.pnlTank6.TabIndex = 16;
            // 
            // lblNo6
            // 
            this.lblNo6.AutoSize = true;
            this.lblNo6.BackColor = System.Drawing.Color.Transparent;
            this.lblNo6.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblNo6.Location = new System.Drawing.Point(65, 4);
            this.lblNo6.Name = "lblNo6";
            this.lblNo6.Size = new System.Drawing.Size(32, 36);
            this.lblNo6.TabIndex = 26;
            this.lblNo6.Text = "6";
            // 
            // lblTank6
            // 
            this.lblTank6.AutoSize = true;
            this.lblTank6.BackColor = System.Drawing.Color.Transparent;
            this.lblTank6.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTank6.Location = new System.Drawing.Point(48, 162);
            this.lblTank6.Name = "lblTank6";
            this.lblTank6.Size = new System.Drawing.Size(55, 29);
            this.lblTank6.TabIndex = 7;
            this.lblTank6.Text = "100";
            // 
            // pnlP6
            // 
            this.pnlP6.BackColor = System.Drawing.Color.Transparent;
            this.pnlP6.Controls.Add(this.pnlPressure6);
            this.pnlP6.Location = new System.Drawing.Point(53, 50);
            this.pnlP6.Name = "pnlP6";
            this.pnlP6.Size = new System.Drawing.Size(50, 100);
            this.pnlP6.TabIndex = 7;
            // 
            // pnlPressure6
            // 
            this.pnlPressure6.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.pnlPressure6.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnlPressure6.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlPressure6.Location = new System.Drawing.Point(0, 0);
            this.pnlPressure6.Name = "pnlPressure6";
            this.pnlPressure6.Size = new System.Drawing.Size(50, 100);
            this.pnlPressure6.TabIndex = 1;
            // 
            // btnValve5
            // 
            this.btnValve5.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnValve5.BackgroundImage")));
            this.btnValve5.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.btnValve5.Location = new System.Drawing.Point(935, 348);
            this.btnValve5.Name = "btnValve5";
            this.btnValve5.Size = new System.Drawing.Size(70, 50);
            this.btnValve5.TabIndex = 14;
            this.btnValve5.UseVisualStyleBackColor = true;
            this.btnValve5.Click += new System.EventHandler(this.btnValve5_Click);
            // 
            // pnlMeter5
            // 
            this.pnlMeter5.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pnlMeter5.BackgroundImage")));
            this.pnlMeter5.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.pnlMeter5.Location = new System.Drawing.Point(852, 268);
            this.pnlMeter5.Name = "pnlMeter5";
            this.pnlMeter5.Size = new System.Drawing.Size(153, 74);
            this.pnlMeter5.TabIndex = 13;
            // 
            // pnlTank5
            // 
            this.pnlTank5.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pnlTank5.BackgroundImage")));
            this.pnlTank5.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.pnlTank5.Controls.Add(this.lblNo5);
            this.pnlTank5.Controls.Add(this.lblTank5);
            this.pnlTank5.Controls.Add(this.pnlP5);
            this.pnlTank5.Location = new System.Drawing.Point(852, 43);
            this.pnlTank5.Name = "pnlTank5";
            this.pnlTank5.Size = new System.Drawing.Size(153, 219);
            this.pnlTank5.TabIndex = 12;
            // 
            // lblNo5
            // 
            this.lblNo5.AutoSize = true;
            this.lblNo5.BackColor = System.Drawing.Color.Transparent;
            this.lblNo5.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblNo5.Location = new System.Drawing.Point(64, 4);
            this.lblNo5.Name = "lblNo5";
            this.lblNo5.Size = new System.Drawing.Size(32, 36);
            this.lblNo5.TabIndex = 26;
            this.lblNo5.Text = "5";
            // 
            // lblTank5
            // 
            this.lblTank5.AutoSize = true;
            this.lblTank5.BackColor = System.Drawing.Color.Transparent;
            this.lblTank5.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTank5.Location = new System.Drawing.Point(50, 162);
            this.lblTank5.Name = "lblTank5";
            this.lblTank5.Size = new System.Drawing.Size(55, 29);
            this.lblTank5.TabIndex = 7;
            this.lblTank5.Text = "100";
            // 
            // pnlP5
            // 
            this.pnlP5.BackColor = System.Drawing.Color.Transparent;
            this.pnlP5.Controls.Add(this.pnlPressure5);
            this.pnlP5.Location = new System.Drawing.Point(55, 50);
            this.pnlP5.Name = "pnlP5";
            this.pnlP5.Size = new System.Drawing.Size(50, 100);
            this.pnlP5.TabIndex = 7;
            // 
            // pnlPressure5
            // 
            this.pnlPressure5.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.pnlPressure5.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnlPressure5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlPressure5.Location = new System.Drawing.Point(0, 0);
            this.pnlPressure5.Name = "pnlPressure5";
            this.pnlPressure5.Size = new System.Drawing.Size(50, 100);
            this.pnlPressure5.TabIndex = 1;
            // 
            // btnValve4
            // 
            this.btnValve4.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnValve4.BackgroundImage")));
            this.btnValve4.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.btnValve4.Location = new System.Drawing.Point(734, 348);
            this.btnValve4.Name = "btnValve4";
            this.btnValve4.Size = new System.Drawing.Size(70, 50);
            this.btnValve4.TabIndex = 11;
            this.btnValve4.UseVisualStyleBackColor = true;
            this.btnValve4.Click += new System.EventHandler(this.btnValve4_Click);
            // 
            // pnlMeter4
            // 
            this.pnlMeter4.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pnlMeter4.BackgroundImage")));
            this.pnlMeter4.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.pnlMeter4.Location = new System.Drawing.Point(651, 268);
            this.pnlMeter4.Name = "pnlMeter4";
            this.pnlMeter4.Size = new System.Drawing.Size(153, 74);
            this.pnlMeter4.TabIndex = 10;
            // 
            // pnlTank4
            // 
            this.pnlTank4.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pnlTank4.BackgroundImage")));
            this.pnlTank4.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.pnlTank4.Controls.Add(this.lblNo4);
            this.pnlTank4.Controls.Add(this.lblTank4);
            this.pnlTank4.Controls.Add(this.pnlP4);
            this.pnlTank4.Location = new System.Drawing.Point(651, 43);
            this.pnlTank4.Name = "pnlTank4";
            this.pnlTank4.Size = new System.Drawing.Size(153, 219);
            this.pnlTank4.TabIndex = 9;
            // 
            // lblNo4
            // 
            this.lblNo4.AutoSize = true;
            this.lblNo4.BackColor = System.Drawing.Color.Transparent;
            this.lblNo4.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblNo4.Location = new System.Drawing.Point(62, 4);
            this.lblNo4.Name = "lblNo4";
            this.lblNo4.Size = new System.Drawing.Size(32, 36);
            this.lblNo4.TabIndex = 26;
            this.lblNo4.Text = "4";
            // 
            // lblTank4
            // 
            this.lblTank4.AutoSize = true;
            this.lblTank4.BackColor = System.Drawing.Color.Transparent;
            this.lblTank4.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTank4.Location = new System.Drawing.Point(49, 162);
            this.lblTank4.Name = "lblTank4";
            this.lblTank4.Size = new System.Drawing.Size(55, 29);
            this.lblTank4.TabIndex = 7;
            this.lblTank4.Text = "100";
            // 
            // pnlP4
            // 
            this.pnlP4.BackColor = System.Drawing.Color.Transparent;
            this.pnlP4.Controls.Add(this.pnlPressure4);
            this.pnlP4.Location = new System.Drawing.Point(54, 50);
            this.pnlP4.Name = "pnlP4";
            this.pnlP4.Size = new System.Drawing.Size(50, 100);
            this.pnlP4.TabIndex = 7;
            // 
            // pnlPressure4
            // 
            this.pnlPressure4.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.pnlPressure4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnlPressure4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlPressure4.Location = new System.Drawing.Point(0, 0);
            this.pnlPressure4.Name = "pnlPressure4";
            this.pnlPressure4.Size = new System.Drawing.Size(50, 100);
            this.pnlPressure4.TabIndex = 1;
            // 
            // btnValve3
            // 
            this.btnValve3.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnValve3.BackgroundImage")));
            this.btnValve3.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.btnValve3.Location = new System.Drawing.Point(538, 348);
            this.btnValve3.Name = "btnValve3";
            this.btnValve3.Size = new System.Drawing.Size(70, 50);
            this.btnValve3.TabIndex = 8;
            this.btnValve3.UseVisualStyleBackColor = true;
            this.btnValve3.Click += new System.EventHandler(this.btnValve3_Click);
            // 
            // pnlMeter3
            // 
            this.pnlMeter3.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pnlMeter3.BackgroundImage")));
            this.pnlMeter3.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.pnlMeter3.Location = new System.Drawing.Point(455, 268);
            this.pnlMeter3.Name = "pnlMeter3";
            this.pnlMeter3.Size = new System.Drawing.Size(153, 74);
            this.pnlMeter3.TabIndex = 7;
            // 
            // pnlTank3
            // 
            this.pnlTank3.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pnlTank3.BackgroundImage")));
            this.pnlTank3.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.pnlTank3.Controls.Add(this.lblNo3);
            this.pnlTank3.Controls.Add(this.lblTank3);
            this.pnlTank3.Controls.Add(this.pnlP3);
            this.pnlTank3.Location = new System.Drawing.Point(455, 43);
            this.pnlTank3.Name = "pnlTank3";
            this.pnlTank3.Size = new System.Drawing.Size(153, 219);
            this.pnlTank3.TabIndex = 6;
            // 
            // lblNo3
            // 
            this.lblNo3.AutoSize = true;
            this.lblNo3.BackColor = System.Drawing.Color.Transparent;
            this.lblNo3.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblNo3.Location = new System.Drawing.Point(63, 3);
            this.lblNo3.Name = "lblNo3";
            this.lblNo3.Size = new System.Drawing.Size(32, 36);
            this.lblNo3.TabIndex = 26;
            this.lblNo3.Text = "3";
            // 
            // lblTank3
            // 
            this.lblTank3.AutoSize = true;
            this.lblTank3.BackColor = System.Drawing.Color.Transparent;
            this.lblTank3.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTank3.Location = new System.Drawing.Point(52, 162);
            this.lblTank3.Name = "lblTank3";
            this.lblTank3.Size = new System.Drawing.Size(55, 29);
            this.lblTank3.TabIndex = 7;
            this.lblTank3.Text = "100";
            // 
            // pnlP3
            // 
            this.pnlP3.BackColor = System.Drawing.Color.Transparent;
            this.pnlP3.Controls.Add(this.pnlPressure3);
            this.pnlP3.Location = new System.Drawing.Point(57, 50);
            this.pnlP3.Name = "pnlP3";
            this.pnlP3.Size = new System.Drawing.Size(50, 100);
            this.pnlP3.TabIndex = 7;
            // 
            // pnlPressure3
            // 
            this.pnlPressure3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.pnlPressure3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnlPressure3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlPressure3.Location = new System.Drawing.Point(0, 0);
            this.pnlPressure3.Name = "pnlPressure3";
            this.pnlPressure3.Size = new System.Drawing.Size(50, 100);
            this.pnlPressure3.TabIndex = 1;
            // 
            // btnValve2
            // 
            this.btnValve2.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnValve2.BackgroundImage")));
            this.btnValve2.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.btnValve2.Location = new System.Drawing.Point(340, 348);
            this.btnValve2.Name = "btnValve2";
            this.btnValve2.Size = new System.Drawing.Size(70, 50);
            this.btnValve2.TabIndex = 5;
            this.btnValve2.UseVisualStyleBackColor = true;
            this.btnValve2.Click += new System.EventHandler(this.btnValve2_Click);
            // 
            // pnlMeter2
            // 
            this.pnlMeter2.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pnlMeter2.BackgroundImage")));
            this.pnlMeter2.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.pnlMeter2.Location = new System.Drawing.Point(257, 268);
            this.pnlMeter2.Name = "pnlMeter2";
            this.pnlMeter2.Size = new System.Drawing.Size(153, 74);
            this.pnlMeter2.TabIndex = 4;
            // 
            // btnValve1
            // 
            this.btnValve1.BackgroundImage = global::PROG8150_Assignment03_SCADA.Properties.Resources.ValveNew;
            this.btnValve1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.btnValve1.Location = new System.Drawing.Point(148, 348);
            this.btnValve1.Name = "btnValve1";
            this.btnValve1.Size = new System.Drawing.Size(70, 50);
            this.btnValve1.TabIndex = 2;
            this.btnValve1.UseVisualStyleBackColor = true;
            this.btnValve1.Click += new System.EventHandler(this.btnValve1_Click);
            // 
            // pnlMeter1
            // 
            this.pnlMeter1.BackgroundImage = global::PROG8150_Assignment03_SCADA.Properties.Resources.NormalMeter;
            this.pnlMeter1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.pnlMeter1.Location = new System.Drawing.Point(65, 268);
            this.pnlMeter1.Name = "pnlMeter1";
            this.pnlMeter1.Size = new System.Drawing.Size(153, 74);
            this.pnlMeter1.TabIndex = 1;
            // 
            // pnlTank1
            // 
            this.pnlTank1.BackgroundImage = global::PROG8150_Assignment03_SCADA.Properties.Resources.TankNew;
            this.pnlTank1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.pnlTank1.Controls.Add(this.lblNo1);
            this.pnlTank1.Controls.Add(this.lblTank1);
            this.pnlTank1.Controls.Add(this.pnlP1);
            this.pnlTank1.Location = new System.Drawing.Point(65, 43);
            this.pnlTank1.Name = "pnlTank1";
            this.pnlTank1.Size = new System.Drawing.Size(153, 219);
            this.pnlTank1.TabIndex = 0;
            // 
            // lblNo1
            // 
            this.lblNo1.AutoSize = true;
            this.lblNo1.BackColor = System.Drawing.Color.Transparent;
            this.lblNo1.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblNo1.Location = new System.Drawing.Point(61, 7);
            this.lblNo1.Name = "lblNo1";
            this.lblNo1.Size = new System.Drawing.Size(32, 36);
            this.lblNo1.TabIndex = 25;
            this.lblNo1.Text = "1";
            // 
            // lblTank1
            // 
            this.lblTank1.AutoSize = true;
            this.lblTank1.BackColor = System.Drawing.Color.Transparent;
            this.lblTank1.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTank1.Location = new System.Drawing.Point(53, 162);
            this.lblTank1.Name = "lblTank1";
            this.lblTank1.Size = new System.Drawing.Size(55, 29);
            this.lblTank1.TabIndex = 6;
            this.lblTank1.Text = "100";
            // 
            // pnlP1
            // 
            this.pnlP1.BackColor = System.Drawing.Color.Transparent;
            this.pnlP1.Controls.Add(this.pnlPressure1);
            this.pnlP1.Location = new System.Drawing.Point(58, 50);
            this.pnlP1.Name = "pnlP1";
            this.pnlP1.Size = new System.Drawing.Size(50, 100);
            this.pnlP1.TabIndex = 6;
            // 
            // pnlPressure1
            // 
            this.pnlPressure1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.pnlPressure1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnlPressure1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlPressure1.Location = new System.Drawing.Point(0, 0);
            this.pnlPressure1.Name = "pnlPressure1";
            this.pnlPressure1.Size = new System.Drawing.Size(50, 100);
            this.pnlPressure1.TabIndex = 1;
            // 
            // pnlTank2
            // 
            this.pnlTank2.BackgroundImage = global::PROG8150_Assignment03_SCADA.Properties.Resources.TankNew;
            this.pnlTank2.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.pnlTank2.Controls.Add(this.lblNo2);
            this.pnlTank2.Controls.Add(this.lblTank2);
            this.pnlTank2.Controls.Add(this.pnlP2);
            this.pnlTank2.Location = new System.Drawing.Point(257, 43);
            this.pnlTank2.Name = "pnlTank2";
            this.pnlTank2.Size = new System.Drawing.Size(153, 219);
            this.pnlTank2.TabIndex = 3;
            // 
            // lblNo2
            // 
            this.lblNo2.AutoSize = true;
            this.lblNo2.BackColor = System.Drawing.Color.Transparent;
            this.lblNo2.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblNo2.Location = new System.Drawing.Point(62, 3);
            this.lblNo2.Name = "lblNo2";
            this.lblNo2.Size = new System.Drawing.Size(32, 36);
            this.lblNo2.TabIndex = 26;
            this.lblNo2.Text = "2";
            // 
            // lblTank2
            // 
            this.lblTank2.AutoSize = true;
            this.lblTank2.BackColor = System.Drawing.Color.Transparent;
            this.lblTank2.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTank2.Location = new System.Drawing.Point(46, 162);
            this.lblTank2.Name = "lblTank2";
            this.lblTank2.Size = new System.Drawing.Size(55, 29);
            this.lblTank2.TabIndex = 7;
            this.lblTank2.Text = "100";
            // 
            // pnlP2
            // 
            this.pnlP2.BackColor = System.Drawing.Color.Transparent;
            this.pnlP2.Controls.Add(this.pnlPressure2);
            this.pnlP2.Location = new System.Drawing.Point(51, 50);
            this.pnlP2.Name = "pnlP2";
            this.pnlP2.Size = new System.Drawing.Size(50, 100);
            this.pnlP2.TabIndex = 7;
            // 
            // pnlPressure2
            // 
            this.pnlPressure2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.pnlPressure2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnlPressure2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlPressure2.Location = new System.Drawing.Point(0, 0);
            this.pnlPressure2.Name = "pnlPressure2";
            this.pnlPressure2.Size = new System.Drawing.Size(50, 100);
            this.pnlPressure2.TabIndex = 1;
            // 
            // frmMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.Control;
            this.ClientSize = new System.Drawing.Size(1276, 832);
            this.Controls.Add(this.pictureBox9);
            this.Controls.Add(this.pictureBox8);
            this.Controls.Add(this.pictureBox7);
            this.Controls.Add(this.pictureBox6);
            this.Controls.Add(this.pictureBox5);
            this.Controls.Add(this.pictureBox4);
            this.Controls.Add(this.pictureBox3);
            this.Controls.Add(this.pictureBox2);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.btnApply);
            this.Controls.Add(this.txtMax);
            this.Controls.Add(this.txtMin);
            this.Controls.Add(this.btnReset8);
            this.Controls.Add(this.btnReset7);
            this.Controls.Add(this.btnReset6);
            this.Controls.Add(this.btnReset5);
            this.Controls.Add(this.btnReset4);
            this.Controls.Add(this.btnReset3);
            this.Controls.Add(this.btnReset2);
            this.Controls.Add(this.btnReset1);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.btnShutDown);
            this.Controls.Add(this.btnValve8);
            this.Controls.Add(this.pnlMeter8);
            this.Controls.Add(this.pnlTank8);
            this.Controls.Add(this.btnValve7);
            this.Controls.Add(this.pnlMeter7);
            this.Controls.Add(this.pnlTank7);
            this.Controls.Add(this.btnValve6);
            this.Controls.Add(this.pnlMeter6);
            this.Controls.Add(this.pnlTank6);
            this.Controls.Add(this.rtbInfor);
            this.Controls.Add(this.btnValve5);
            this.Controls.Add(this.pnlMeter5);
            this.Controls.Add(this.pnlTank5);
            this.Controls.Add(this.btnValve4);
            this.Controls.Add(this.pnlMeter4);
            this.Controls.Add(this.pnlTank4);
            this.Controls.Add(this.btnValve3);
            this.Controls.Add(this.pnlMeter3);
            this.Controls.Add(this.pnlTank3);
            this.Controls.Add(this.btnValve2);
            this.Controls.Add(this.pnlMeter2);
            this.Controls.Add(this.btnValve1);
            this.Controls.Add(this.pnlMeter1);
            this.Controls.Add(this.pnlTank1);
            this.Controls.Add(this.mnMain);
            this.Controls.Add(this.pnlTank2);
            this.MainMenuStrip = this.mnMain;
            this.MaximizeBox = false;
            this.Name = "frmMain";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "ISS Oxygen System";
            this.mnMain.ResumeLayout(false);
            this.mnMain.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.pnlTank8.ResumeLayout(false);
            this.pnlTank8.PerformLayout();
            this.pnlP8.ResumeLayout(false);
            this.pnlTank7.ResumeLayout(false);
            this.pnlTank7.PerformLayout();
            this.pnlP7.ResumeLayout(false);
            this.pnlTank6.ResumeLayout(false);
            this.pnlTank6.PerformLayout();
            this.pnlP6.ResumeLayout(false);
            this.pnlTank5.ResumeLayout(false);
            this.pnlTank5.PerformLayout();
            this.pnlP5.ResumeLayout(false);
            this.pnlTank4.ResumeLayout(false);
            this.pnlTank4.PerformLayout();
            this.pnlP4.ResumeLayout(false);
            this.pnlTank3.ResumeLayout(false);
            this.pnlTank3.PerformLayout();
            this.pnlP3.ResumeLayout(false);
            this.pnlTank1.ResumeLayout(false);
            this.pnlTank1.PerformLayout();
            this.pnlP1.ResumeLayout(false);
            this.pnlTank2.ResumeLayout(false);
            this.pnlTank2.PerformLayout();
            this.pnlP2.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Panel pnlTank1;
        private System.Windows.Forms.Panel pnlPressure1;
        private System.Windows.Forms.Panel pnlMeter1;
        private System.Windows.Forms.Button btnValve1;
        private System.Windows.Forms.Button btnValve2;
        private System.Windows.Forms.Panel pnlMeter2;
        private System.Windows.Forms.Panel pnlTank2;
        private System.Windows.Forms.Timer timer1;
        private System.Windows.Forms.Panel pnlP1;
        private System.Windows.Forms.Panel pnlP2;
        private System.Windows.Forms.Panel pnlPressure2;
        private System.Windows.Forms.Label lblTank1;
        private System.Windows.Forms.Label lblTank2;
        private System.Windows.Forms.Button btnValve3;
        private System.Windows.Forms.Panel pnlMeter3;
        private System.Windows.Forms.Panel pnlTank3;
        private System.Windows.Forms.Label lblTank3;
        private System.Windows.Forms.Panel pnlP3;
        private System.Windows.Forms.Panel pnlPressure3;
        private System.Windows.Forms.Button btnValve4;
        private System.Windows.Forms.Panel pnlMeter4;
        private System.Windows.Forms.Panel pnlTank4;
        private System.Windows.Forms.Label lblTank4;
        private System.Windows.Forms.Panel pnlP4;
        private System.Windows.Forms.Panel pnlPressure4;
        private System.Windows.Forms.Button btnValve5;
        private System.Windows.Forms.Panel pnlMeter5;
        private System.Windows.Forms.Panel pnlTank5;
        private System.Windows.Forms.Label lblTank5;
        private System.Windows.Forms.Panel pnlP5;
        private System.Windows.Forms.Panel pnlPressure5;
        private System.Windows.Forms.Label lblNo1;
        private System.Windows.Forms.RichTextBox rtbInfor;
        private System.Windows.Forms.Button btnValve8;
        private System.Windows.Forms.Panel pnlMeter8;
        private System.Windows.Forms.Panel pnlTank8;
        private System.Windows.Forms.Label lblTank8;
        private System.Windows.Forms.Panel pnlP8;
        private System.Windows.Forms.Panel pnlPressure8;
        private System.Windows.Forms.Button btnValve7;
        private System.Windows.Forms.Panel pnlMeter7;
        private System.Windows.Forms.Panel pnlTank7;
        private System.Windows.Forms.Label lblTank7;
        private System.Windows.Forms.Panel pnlP7;
        private System.Windows.Forms.Panel pnlPressure7;
        private System.Windows.Forms.Button btnValve6;
        private System.Windows.Forms.Panel pnlMeter6;
        private System.Windows.Forms.Panel pnlTank6;
        private System.Windows.Forms.Label lblTank6;
        private System.Windows.Forms.Panel pnlP6;
        private System.Windows.Forms.Panel pnlPressure6;
        private System.Windows.Forms.Label lblNo2;
        private System.Windows.Forms.Label lblNo3;
        private System.Windows.Forms.Label lblNo4;
        private System.Windows.Forms.Label lblNo5;
        private System.Windows.Forms.Label lblNo8;
        private System.Windows.Forms.Label lblNo7;
        private System.Windows.Forms.Label lblNo6;
        private System.Windows.Forms.Timer timerBlink;
        private System.Windows.Forms.Timer timerAlarm;
        private System.Windows.Forms.Button btnShutDown;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button btnReset1;
        private System.Windows.Forms.Button btnReset2;
        private System.Windows.Forms.Button btnReset3;
        private System.Windows.Forms.Button btnReset4;
        private System.Windows.Forms.Button btnReset5;
        private System.Windows.Forms.Button btnReset6;
        private System.Windows.Forms.Button btnReset7;
        private System.Windows.Forms.Button btnReset8;
        private System.Windows.Forms.MenuStrip mnMain;
        private System.Windows.Forms.ToolStripMenuItem oxygenTankMapToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem manualGuideToolStripMenuItem;
        private System.Windows.Forms.TextBox txtMin;
        private System.Windows.Forms.TextBox txtMax;
        private System.Windows.Forms.Button btnApply;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.PictureBox pictureBox3;
        private System.Windows.Forms.PictureBox pictureBox4;
        private System.Windows.Forms.PictureBox pictureBox5;
        private System.Windows.Forms.PictureBox pictureBox6;
        private System.Windows.Forms.PictureBox pictureBox7;
        private System.Windows.Forms.PictureBox pictureBox8;
        private System.Windows.Forms.PictureBox pictureBox9;
    }
}

