﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Windows.Forms;

namespace PROG8150_Assignment03_SCADA
{
    public partial class frmMain : Form
    {
        Random rdn = new Random();
        public static int minAlarm = 20, maxAlarm = 80; 
        public static int alarmSound;
        public static System.Media.SoundPlayer player = new System.Media.SoundPlayer();
        public static List<Tank> listTank = new List<Tank>();
        public Tank tank1;
        public Tank tank2;
        public Tank tank3;
        public Tank tank4;
        public Tank tank5;
        public Tank tank6;
        public Tank tank7;
        public Tank tank8;

        public frmMain()
        {
            InitializeComponent();
            initializeState();
        }

        private void btnValve1_Click(object sender, EventArgs e)
        {
            valveONOFF(tank1);
        }

        private void btnValve2_Click(object sender, EventArgs e)
        {
            valveONOFF(tank2);
        }

        private void btnValve3_Click(object sender, EventArgs e)
        {
            valveONOFF(tank3);
        }

        private void btnValve4_Click(object sender, EventArgs e)
        {
            valveONOFF(tank4);
        }

        private void btnValve5_Click(object sender, EventArgs e)
        {
            valveONOFF(tank5);
        }

        private void btnValve6_Click(object sender, EventArgs e)
        {
            valveONOFF(tank6);
        }

        private void btnValve7_Click(object sender, EventArgs e)
        {
            valveONOFF(tank7);
        }

        private void btnValve8_Click(object sender, EventArgs e)
        {
            valveONOFF(tank8);
        }

        private void btnShutDown_Click(object sender, EventArgs e)
        {
            if (btnShutDown.Text == "SHUT DOWN")
            {
                foreach(Tank tank in listTank)
                {
                    tank.ValveState = false;
                    tank.BtnValve.Enabled = false;
                    tank.BtnValve.BackgroundImage = Properties.Resources.ValveClosed;
                    tank.BtnValve.BackColor = Color.Transparent;
                    tank.BtnReset.Text = "-";
                }
                timer1.Enabled = false;
                timer1.Stop();
                timer1.Dispose();
                timerBlink.Enabled = false;
                timerBlink.Stop();
                timerBlink.Dispose();
                timerAlarm.Enabled = false;
                timerAlarm.Dispose();
                timerAlarm.Stop();
                player.Stop();
                btnShutDown.Text = "RESTART";
                MessageBox.Show("SYSTEM IS SHUT DOWN", "WARNING", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                rtbInfor.AppendText(string.Format("{0} - SYSTEM SHUTDOWN", DateTime.Now));
                listTank.Clear();
            }
            else
            {
                MessageBox.Show("SYSTEM IS RESTARTED", "WARNING", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                initializeState();
            }
        }

        private void valveONOFF(Tank tank)
        {
            if (tank.ValveState == true)
            {
                tank.BtnValve.BackColor = Color.Transparent;
                tank.BtnValve.BackgroundImage = Properties.Resources.ValveClosed;
                tank.BtnValve.BackgroundImageLayout = ImageLayout.Zoom;
                tank.ValveState = false;
            }
            else
            {
                tank.BtnValve.BackColor = Color.LightGreen;
                tank.BtnValve.BackgroundImage = Properties.Resources.ValveNew;
                tank.BtnValve.BackgroundImageLayout = ImageLayout.Zoom;
                tank.ValveState = true;
            }

        }

        private void timerAlarm_Tick(object sender, EventArgs e)
        {
            alarmSound = alertCheckingAll();
            if (alarmSound == 1)
            {
                player.PlayLooping();
            }
            else if (alarmSound == 0)
            {
                player.Stop();
            }
        }

        private int alertCheckingAll()
        {
            if (tank1.AlarmState == false && tank2.AlarmState == false && tank3.AlarmState == false && tank4.AlarmState == false && tank5.AlarmState == false && tank6.AlarmState == false && tank7.AlarmState == false && tank8.AlarmState == false)
                alarmSound = 0;
            else
                alarmSound = 1;
            return alarmSound;
        }

        public void initializeState()
        {
            //initialize timer for all
            timer1.Interval = 5000;
            timer1.Enabled = true;
            timer1.Start();

            //timer for Blink
            timerBlink.Interval = 500;
            timerBlink.Enabled = true;
            timerBlink.Start();

            //alarm sound
            timerAlarm.Interval = 1000;
            timerAlarm.Enabled = true;
            timerAlarm.Start();
            player.Stream = Properties.Resources.ALARM2;

            rtbInfor.Clear();
            btnShutDown.Text = "SHUT DOWN";

            //initialize all valve / tank pressure / meter state
            tank1 = new Tank(pnlTank1, pnlMeter1, pnlPressure1, pnlP1, lblNo1, lblTank1, btnValve1, btnReset1, true, false);
            tank2 = new Tank(pnlTank2, pnlMeter2, pnlPressure2, pnlP2, lblNo2, lblTank2, btnValve2, btnReset2, true, false);
            tank3 = new Tank(pnlTank3, pnlMeter3, pnlPressure3, pnlP3, lblNo3, lblTank3, btnValve3, btnReset3, true, false);
            tank4 = new Tank(pnlTank4, pnlMeter4, pnlPressure4, pnlP4, lblNo4, lblTank4, btnValve4, btnReset4, true, false);
            tank5 = new Tank(pnlTank5, pnlMeter5, pnlPressure5, pnlP5, lblNo5, lblTank5, btnValve5, btnReset5, true, false);
            tank6 = new Tank(pnlTank6, pnlMeter6, pnlPressure6, pnlP6, lblNo6, lblTank6, btnValve6, btnReset6, true, false);
            tank7 = new Tank(pnlTank7, pnlMeter7, pnlPressure7, pnlP7, lblNo7, lblTank7, btnValve7, btnReset7, true, false);
            tank8 = new Tank(pnlTank8, pnlMeter8, pnlPressure8, pnlP8, lblNo8, lblTank8, btnValve8, btnReset8, true, false);
            listTank.Add(tank1);
            listTank.Add(tank2);
            listTank.Add(tank3);
            listTank.Add(tank4);
            listTank.Add(tank5);
            listTank.Add(tank6);
            listTank.Add(tank7);
            listTank.Add(tank8);

            foreach(Tank tank in listTank)
            {
                componentInitialize(tank, true);
            }

        }

        private void componentInitialize(Tank tank, bool restart)
        {
            tank.PnlPressure.Dock = DockStyle.Bottom;
            if(restart == true)
            {
                tank.PnlMeter.BackgroundImage = Properties.Resources.NormalMeter;
                tank.PnlPressure.Height = 50;
                tank.LblPressure.Text = tank.PnlPressure.Height.ToString();
                tank.BtnValve.BackColor = Color.LightGreen;
                tank.ValveState = true;
                tank.BtnValve.Enabled = true;
                tank.BtnValve.BackgroundImage = Properties.Resources.ValveNew;
                tank.BtnReset.Text = "OK";
                tank.BtnReset.BackgroundImage = null;
            }
            tank.AlarmState = false;
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            foreach(Tank tank in listTank)
            {
                if (tank.ValveState == true)
                {
                    timerRunTime(tank);
                }
                else if (tank.ValveState == false)
                {
                    componentInitialize(tank, false);
                }
            }
        }

        private void timerRunTime(Tank tank)
        {
            int change = rdn.Next(-20, 20);
            int height = 0;
            tank.PnlPressure.Height = tank.PnlPressure.Height + change;
            if (tank.PnlPressure.Height > 100) tank.PnlPressure.Height = 100;
            else if (tank.PnlPressure.Height < 0) tank.PnlPressure.Height = 0;
            tank.LblPressure.Text = tank.PnlPressure.Height.ToString();
            height = int.Parse(tank.LblPressure.Text);
            if (height < minAlarm || height > maxAlarm)
            {
                tank.AlarmState = true;
                if (height < minAlarm)
                {
                    recordAlert(tank.LblNo, true);
                    tank.PnlMeter.BackgroundImage = Properties.Resources.LowMeter;
                    tank.BtnReset.Text = "";
                    tank.BtnReset.BackgroundImage = Properties.Resources.upArrow;
                    tank.BtnReset.BackgroundImageLayout = ImageLayout.Zoom;
                }
                else if (height > maxAlarm)
                {
                    recordAlert(tank.LblNo, false);
                    tank.PnlMeter.BackgroundImage = Properties.Resources.HighMeter;
                    tank.BtnReset.Text = "";
                    tank.BtnReset.BackgroundImage = Properties.Resources.downArrow;
                    tank.BtnReset.BackgroundImageLayout = ImageLayout.Zoom;
                }
            }
            else
            {
                tank.AlarmState = false;
                tank.PnlMeter.BackgroundImage = Properties.Resources.NormalMeter;
                tank.BtnReset.Text = "OK";
                tank.BtnReset.BackgroundImage = null;
            }
        }

        private void recordAlert(Label lblNo, bool low)
        {
            if (low == true)
            {
                rtbInfor.SelectionBackColor = Color.Yellow;
                rtbInfor.AppendText(string.Format("{0} - Tank No: {1} : LOW PRESSURE\r\n", DateTime.Now, lblNo.Text));
                rtbInfor.AppendText(string.Format("---------------------------------\r\n"));
                rtbInfor.ScrollToCaret();
            }
            else
            {
                rtbInfor.SelectionColor = Color.Red;
                rtbInfor.AppendText(string.Format("{0} - Tank No: {1} : HIGH PRESSURE\r\n", DateTime.Now, lblNo.Text));
                rtbInfor.AppendText(string.Format("---------------------------------\r\n"));
                rtbInfor.ScrollToCaret();
            }
        }

        private void timerBlink_Tick(object sender, EventArgs e)
        {
            foreach (Tank tank in listTank)
            {
                checkBlink(tank);
            }
        }

        private void checkBlink(Tank tank)
        {
            if(tank.ValveState == true)
            {
                if(tank.AlarmState == true)
                {
                    if (int.Parse(tank.LblPressure.Text) < minAlarm)
                    {
                        switchColor(tank, "low");
                    }
                    else if (int.Parse(tank.LblPressure.Text) > maxAlarm)
                    {
                        switchColor(tank, "high");
                    }
                }
                else
                {
                    tank.PnlTank.BackColor = Color.Transparent;
                }
            }
        }

        private void btnReset1_Click(object sender, EventArgs e)
        {
            componentInitialize(tank1, true);
        }

        private void btnReset2_Click(object sender, EventArgs e)
        {
            componentInitialize(tank2, true);
        }

        private void btnReset3_Click(object sender, EventArgs e)
        {
            componentInitialize(tank3, true);
        }

        private void btnReset4_Click(object sender, EventArgs e)
        {
            componentInitialize(tank4, true);
        }

        private void btnReset5_Click(object sender, EventArgs e)
        {
            componentInitialize(tank5, true);
        }

        private void btnReset6_Click(object sender, EventArgs e)
        {
            componentInitialize(tank6, true);
        }

        private void btnReset7_Click(object sender, EventArgs e)
        {
            componentInitialize(tank7, true);
        }

        private void btnReset8_Click(object sender, EventArgs e)
        {
            componentInitialize(tank8, true);
        }

        private void oxygenTankMapToolStripMenuItem_Click(object sender, EventArgs e)
        {
            new frmMap().Show(); 
        }

        private void txtMin_TextChanged(object sender, EventArgs e)
        {
            int temp = 0;
            if (!int.TryParse(txtMin.Text, out temp) || temp < 0)
            {
                txtMin.BackColor = Color.Red;
                txtMin.Focus();
            }
            else
            {
                txtMin.BackColor = Color.White;
            }
        }

        private void txtMax_TextChanged(object sender, EventArgs e)
        {
            int temp = 0;
            if (!int.TryParse(txtMax.Text, out temp) || temp > 100)
            {
                txtMax.BackColor = Color.Red;
                txtMax.Focus();
            }
            else
            {
                txtMax.BackColor = Color.White;
            }
        }

        private void btnApply_Click(object sender, EventArgs e)
        {
            int tempMin, tempMax;
            tempMax = tempMin = 0;
            if(int.TryParse(txtMin.Text, out tempMin) == false || tempMin < 0)
            {
                txtMin.BackColor = Color.Red;
                txtMin.Focus();
            }
            else
            {
                txtMin.BackColor = Color.White;
                minAlarm = tempMin;
                MessageBox.Show(string.Format("Minimum changed to {0}", minAlarm.ToString()),"Warning",MessageBoxButtons.OK,MessageBoxIcon.Warning);
            }
            if(int.TryParse(txtMax.Text, out tempMax) == false || tempMax > 100)
            {
                txtMax.BackColor = Color.Red;
                txtMax.Focus();
            }
            else
            {
                txtMax.BackColor = Color.White;
                maxAlarm = tempMax;
                MessageBox.Show(string.Format("Maximum changed to {0}", maxAlarm.ToString()),"Warning",MessageBoxButtons.OK,MessageBoxIcon.Warning);
            }
        }

        private void manualGuideToolStripMenuItem_Click(object sender, EventArgs e)
        {
            new frmUserGuide().Show();
        }

        private void switchColor(Tank tank, string alertType)
        {
            if(alertType == "low")
            {
                if (tank.PnlTank.BackColor == Color.Yellow)
                    tank.PnlTank.BackColor = Color.Transparent;
                else
                    tank.PnlTank.BackColor = Color.Yellow;
            }
            else if (alertType == "high")
            {
                if (tank.PnlTank.BackColor == Color.Red)
                    tank.PnlTank.BackColor = Color.Transparent;
                else
                    tank.PnlTank.BackColor = Color.Red;
            }
        }
    }
}

public class Tank
{
    Panel pnlTank;
    Panel pnlMeter;
    Panel pnlPressure;
    Panel pnlP; // contain Pressure panel
    Label lblNo;
    Label lblPressure;
    Button btnValve;
    Button btnReset;

    bool valveState;
    bool alarmState;

    public Tank()
    {
    }

    public Tank(Panel pnlTank, Panel pnlMeter, Panel pnlPressure, Panel pnlP, Label lblNo, Label lblPressure, Button btnValve, Button btnReset)
    {
        this.pnlTank = pnlTank;
        this.pnlMeter = pnlMeter;
        this.pnlPressure = pnlPressure;
        this.pnlP = pnlP;
        this.lblNo = lblNo;
        this.lblPressure = lblPressure;
        this.btnValve = btnValve;
        this.btnReset = btnReset;
    }

    public Tank(Panel pnlTank, Panel pnlMeter, Panel pnlPressure, Panel pnlP, Label lblNo, Label lblPressure, Button btnValve, Button btnReset, bool valveState, bool alarmState) : this(pnlTank, pnlMeter, pnlPressure, pnlP, lblNo, lblPressure, btnValve, btnReset)
    {
        this.valveState = valveState;
        this.alarmState = alarmState;
    }

    public Panel PnlTank { get => pnlTank; set => pnlTank = value; }
    public Panel PnlMeter { get => pnlMeter; set => pnlMeter = value; }
    public Panel PnlPressure { get => pnlPressure; set => pnlPressure = value; }
    public Panel PnlP { get => pnlP; set => pnlP = value; }
    public Label LblNo { get => lblNo; set => lblNo = value; }
    public Label LblPressure { get => lblPressure; set => lblPressure = value; }
    public Button BtnValve { get => btnValve; set => btnValve = value; }
    public bool ValveState { get => valveState; set => valveState = value; }
    public bool AlarmState { get => alarmState; set => alarmState = value; }
    public Button BtnReset { get => btnReset; set => btnReset = value; }
}
