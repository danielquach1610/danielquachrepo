﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Flyweight
{
    class Program
    {
        /// <summary>
        /// entry point for game simulation
        /// </summary>
        static void Main(string[] args)
        {
            //instantiate some characters
            string[] gameCharacter = { "Monk", "Warrior", "Archer", "Warrior", "Archer", "Monk" };

            //initial current relic's amount
            int relic = 0;

            //for each character use flyweight object
            foreach(var character in gameCharacter)
            {
                relic++;
                Party p = PartyFactory.GetParty(character);
                p.RelicFound(relic);
            }

            Console.ReadKey();
        }
    }

    /// <summary>
    /// abstract class for game -- Flyweight abstract class
    /// </summary>
    abstract class Party
    {
        protected string character;
        protected int relic;
        public abstract void RelicFound(int relic);
    }

    /// <summary>
    /// Flyweight concrete class - extend from abstract
    /// </summary>
    class Monk : Party
    {
        public Monk()
        {
            character = "Monk";
        }
        public override void RelicFound(int relic)
        {
            this.relic = relic;
            Console.WriteLine("{0} found a relic --- Total relic: {1}", character, this.relic);
        }
    }

    /// <summary>
    /// Flyweight concrete class - extend from abstract
    /// </summary>
    class Warrior : Party
    {
        public Warrior()
        {
            character = "Warrior";
        }

        public override void RelicFound(int relic)
        {
            this.relic = relic;
            Console.WriteLine("{0} found a relic --- Total relic: {1}", character, this.relic);
        }
    }

    /// <summary>
    /// Flyweight concrete class - extend from abstract
    /// </summary>
    class Archer : Party
    {
        public Archer()
        {
            character = "Archer";
        }

        public override void RelicFound(int relic)
        {
            this.relic = relic;
            Console.WriteLine("{0} found a relic --- Total relic: {1}", character, this.relic);
        }
    }

    /// <summary>
    /// the Flyweight factory
    /// </summary>
    class PartyFactory
    {
        private static Dictionary<string, Party> partyFactory = new Dictionary<string, Party>();
        public static Party GetParty(string input)
        {
            Party p = null;
            if (partyFactory.ContainsKey(input))
            {
                p = partyFactory[input];
            }
            else
            {
                switch(input)
                {
                    case "Monk":
                        p = new Monk();
                        break;
                    case "Warrior":
                        p = new Warrior();
                        break;
                    case "Archer":
                        p = new Archer();
                        break;
                    default:
                        break;
                }
                partyFactory.Add(input, p);
            }
            return p;
        }
    }
}
