﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProxyPattern
{
    class Program
    {
        static void Main(string[] args)
        {
            //create Character Proxy
            CharacterProxy cp = new CharacterProxy();

            cp.KillMonster(50);
            cp.KillMonster(60);
            cp.Die();
            cp.KillMonster(60);
            cp.KillMonster(55);
            cp.KillMonster(45);

            Console.ReadKey();
        }
    }

    /// <summary>
    /// Subject interface
    /// </summary>
    public interface ICharacter
    {
        void KillMonster(int expLoot);
        void Die();
        void LevelUp();
    }

    /// <summary>
    /// Real subject class
    /// </summary>
    class Character : ICharacter
    {
        const int BASE_EXP = 100;
        int exp;
        int level;

        public int Exp { get => exp; set => exp = value; }
        public int Level { get => level; set => level = value; }

        public Character()
        {
            exp = 0;
            level = 1;
            Console.WriteLine("New character. Level: {0}. EXP: {1}", level, exp);
        }

        public void Die()
        {
            Exp = Exp / 2;
            Console.WriteLine("Character died. EXP after penalty: {0}", Exp);
        }

        public void KillMonster(int expLoot)
        {
            Exp += expLoot;
            Console.WriteLine("Character killed monster, earn EXP: {0}. Current EXP: {1}/{2}", expLoot, Exp, Level * BASE_EXP);
            if (Exp >= (Level * BASE_EXP))
            {
                LevelUp();
            }
        }

        public void LevelUp()
        {
            Level++;
            Console.WriteLine("Character level up. Current Level: {0}", Level);
        }
    }

    /// <summary>
    /// Proxy Subject class
    /// </summary>
    class CharacterProxy : ICharacter
    {
        private Character c = new Character();
        public void Die()
        {
            c.Die();
        }

        public void KillMonster(int expLoot)
        {
            c.KillMonster(expLoot);
        }

        public void LevelUp()
        {
           c.LevelUp();
        }
    }
}
