﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MementoPattern
{
    class Program
    {
        static void Main(string[] args)
        {
            Player p = new Player();
            p.Level = 1;
            Console.WriteLine("New Player. Level[{0}]", p.Level);
            Console.WriteLine("Dice > 6 -> Advance | Dice < 6 -> Stay\n");
            Random r = new Random();

            //assume player play through 10 round
            //player has 50% to save new level, 50% to restore old level
            ProspectMemory memory = new ProspectMemory();
            //save initial level
            Console.WriteLine("Save initial level");
            memory.Memento = p.SavePlayerState();
            Console.WriteLine("");

            for (int i = 0; i < 10; i++)
            {
                int round = i;
                Console.WriteLine("\n___Round {0}___", ++round);
                int result = r.Next(1, 12);
                Console.WriteLine("New level: {0}",++p.Level);
                if (result > 6)
                {
                    Console.Write("Dice: {0}. Advance successfully. ", result);
                    memory.Memento = p.SavePlayerState();
                    Console.WriteLine("Level [{0}]\n", p.Level);
                }
                else
                {
                    Console.Write("Dice: {0}. Advance failed. ", result);
                    p.RestorePlayerState(memory.Memento);
                    Console.WriteLine("Level [{0}]\n", p.Level);
                }
            }

            Console.ReadKey();
        }
    }

    /// <summary>
    /// Originator class
    /// </summary>
    class Player
    {
        private int level;

        public int Level
        {
            get { return level; }
            set
            {
                level = value;
            }
        }

        //Store memento
        public PlayerMemento SavePlayerState()
        {
            Console.Write("\nSaving Player State ---");
            return new PlayerMemento(level);
        }

        //Restore memento
        public void RestorePlayerState(PlayerMemento playerMemento)
        {
            Console.Write("\nRestoring Player State ---");
            this.level = playerMemento.Level;
        }
    }

    /// <summary>
    /// Memento class
    /// </summary>
    class PlayerMemento
    {
        private int level;

        public PlayerMemento(int level)
        {
            this.Level = level;
        }

        public int Level { get => level; set => level = value; }
    }


    /// <summary>
    /// Caretaker class
    /// </summary>
    class ProspectMemory
    {
        private PlayerMemento _memento;

        public PlayerMemento Memento { get => _memento; set => _memento = value; }
    }
}
