﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PROG8145_Assignment02
{
    class Program
    {
        static void Main(string[] args)
        {
            Program program = new Program();
            program.Execute();
            Console.ReadKey();
        }

        public void Execute()
        {
            Console.WriteLine("Vehicle machine appointment application");
            Console.WriteLine("***************************************");
            string numberAppointmentString = string.Empty;
            int numberAppointment = 0;

            do
            {
                Console.Write("Enter number of appointment: ");
                numberAppointmentString = Console.ReadLine();
            } while (!int.TryParse(numberAppointmentString, out numberAppointment) || numberAppointment < 0);

            Vehicles[] vehiclesArray = new Vehicles[numberAppointment];
            for (int i = 0; i < numberAppointment; i++)
            {
                vehiclesArray[i] = VehicleRegistration(i);
            }
            Console.WriteLine("==============================================================================================");
            for (int i = 0; i < numberAppointment; i++)
            {
                int cusNum = i+1;
                Console.WriteLine("Customer {0} Name [{1}]", cusNum , vehiclesArray[i].CustomerName);
                Console.WriteLine("Type: {0}    /   Year of Making: {1}     /   Manufacturer: {2}   /   Model: {3}",vehiclesArray[i].Type, vehiclesArray[i].YearOfMaking, vehiclesArray[i].Manufacturer, vehiclesArray[i].ModelVehicle);
                Console.WriteLine("Operation performed:");
                for (int j = 0; j < vehiclesArray[i].Service.Count; j++)
                {
                    Console.WriteLine("{0} / {1}",j, vehiclesArray[i].Service[j]);
                }
                Console.WriteLine("*-----------------*");
            }
            Console.WriteLine("==============================================================================================");
            Console.WriteLine("");
        }

        public Vehicles VehicleRegistration(int slotNumber)
        {
            Vehicles vehicle = new Vehicles();
            Console.WriteLine("==================");
            do
            {
                Console.Write("Customer {0} Name: ", slotNumber+1);
                vehicle.CustomerName = Console.ReadLine();
            } while (vehicle.CustomerName == string.Empty);

            vehicle.YearOfMaking = getYearOfMaking();
            vehicle.Manufacturer = getManufacturer();
            vehicle.ModelVehicle = getModel();

            //get vehicle type
            string[] manufacturerStringList = { "Cars", "School Buses", "Pickup Truck", "Tractor" };
            string vehicleTypeString = string.Empty;
            int vehicleType = 0;

            Console.WriteLine("------------------------------------------------------------------------");
            do
            {
                for (int i = 0; i < manufacturerStringList.Length; i++)
                {
                    Console.Write("{0}. {1} / ", i, manufacturerStringList[i]);
                }
                Console.WriteLine();
                Console.Write("Select vehicle type by enter coresponding number: ");
                vehicleTypeString = Console.ReadLine();
            } while (!(int.TryParse(vehicleTypeString, out vehicleType)) || (vehicleType < 0) || (vehicleType > 3));
            List<string> serviceSelectedList = new List<string>();
            if (vehicleType == 0)
            {
                vehicle.Type = "Cars";
                serviceSelectedList = getService(0);
            }
            else if (vehicleType == 1)
            {
                vehicle.Type = "School Buses";
                serviceSelectedList = getService(1);
            }
            else if (vehicleType == 2)
            {
                vehicle.Type = "Pickup Trucks";
                serviceSelectedList = getService(2);
            }
            else
            {
                vehicle.Type = "Tractors";
                serviceSelectedList = getService(3);
            }
            vehicle.Service = serviceSelectedList;
            return vehicle;
        }

        public int getYearOfMaking()
        {
            //get info year of making
            string yearString = string.Empty;
            int yearOfMaking = 0;
            int currentYear = int.Parse(DateTime.Now.ToString("yyyy"));
            Console.WriteLine("------------------------------------------------------------------------");
            do
            {
                Console.Write("Manufactured Year (1900 < Year < current year): ");
                yearString = Console.ReadLine();
            } while (!int.TryParse(yearString, out yearOfMaking) || yearOfMaking < 1900 || yearOfMaking > currentYear);
            return yearOfMaking;
        }

        public string getManufacturer()
        {
            //get info Manufacturer
            string[] manufacturerStringList = { "Toyota", "Benz", "BMW", "Honda" };
            string manufacturerString = string.Empty;
            int manufacturerBrand = 0;
            Console.WriteLine("------------------------------------------------------------------------");
            do
            {
                for (int i = 0; i < manufacturerStringList.Length; i++)
                {
                    Console.Write("{0}. {1} / ", i, manufacturerStringList[i]);
                }
                Console.WriteLine("4. Other (Please specify)");
                Console.Write("Select manufacturer in list above by entering coresponding number: ");
                manufacturerString = Console.ReadLine();
            } while (!(int.TryParse(manufacturerString, out manufacturerBrand)) || (manufacturerBrand < 0) || (manufacturerBrand > 4));
            if (manufacturerBrand == 4)
            {
                do
                {
                    Console.Write("Specify other Manufacturer Name: ");
                    manufacturerString = Console.ReadLine();
                } while (manufacturerString == string.Empty);
            }
            else
            {
                manufacturerString = manufacturerStringList[manufacturerBrand];
            }
            return manufacturerString;
        }

        public string getModel()
        {
            string[] modelVehicleStringList = { "Skyline", "Hummel", "SUV", "Sedan" };
            string modelString = string.Empty;
            int model = 0;

            //get info Model
            Console.WriteLine("------------------------------------------------------------------------");
            do
            {
                for (int i = 0; i < modelVehicleStringList.Length; i++)
                {
                    Console.Write("{0}. {1} / ", i, modelVehicleStringList[i]);
                }
                Console.WriteLine("4. Other (Please specify)");
                Console.Write("Select model in list above by entering coresponding number: ");
                modelString = Console.ReadLine();
            } while (!(int.TryParse(modelString, out model)) || (model < 0) || (model > 4));
            if (model == 4)
            {
                do
                {
                    Console.Write("Specify other Model Name: ");
                    modelString = Console.ReadLine();
                } while (modelString == string.Empty);
            }
            else
            {
                modelString = modelVehicleStringList[model];
            }
            return modelString;
        }

        public List<string> getService(int vehicleType)
        {
            //get info Service
            Console.WriteLine("0. No Service / 1. Oil Change / 2. Engine Tuneup / 3. Transmission Cleanup");
            Vehicles vehicles = new Vehicles();
            List<string> serviceSelected = new List<string>();
            string serviceString = string.Empty;
            int service = 0;
            Console.WriteLine("------------------------------------------------------------------------");
            do
            {
                Console.Write("Select number coresponding with service: ");
                serviceString = Console.ReadLine();
            } while (!int.TryParse(serviceString, out service) || service < 0 || service > 3);
            switch (service)
            {
                case 1:
                    serviceSelected.Add("Optional Task: " + vehicles.OilChange());
                    break;
                case 2:
                    serviceSelected.Add("Optional Task: " + vehicles.EngineTuneup());
                    break;
                case 3:
                    serviceSelected.Add("Optional Task: " + vehicles.TransmissionCleanup());
                    break;
                default:
                    break;
            }
            switch (vehicleType)
            {
                case 0:
                    Vehicles cars = new Cars();
                    serviceSelected.Add("Required Task: " + ((Cars)cars).SpecificRequiredTask());
                    break;
                case 1:
                    Vehicles bus = new SchoolBuses();
                    serviceSelected.Add("Required Task: " + ((SchoolBuses)bus).SpecificRequiredTask());
                    break;
                case 2:
                    Vehicles truck = new PickupTrucks();
                    serviceSelected.Add("Required Task: " + ((PickupTrucks)truck).SpecificRequiredTask());
                    break;
                case 3:
                    Vehicles tractor = new Tractors();
                    serviceSelected.Add("Required Task: " + ((Tractors)tractor).SpecificRequiredTask());
                    break;
                default:
                    
                    break;
            }
            return serviceSelected;
        }
    }
}
