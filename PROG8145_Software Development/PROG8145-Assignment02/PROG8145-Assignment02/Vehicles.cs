﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PROG8145_Assignment02
{
    class Vehicles
    {
        private int yearOfMaking;
        private string manufacturer;
        private string modelVehicle;
        private List<string> service;
        private string type;
        private string customerName;

        public int YearOfMaking { get => yearOfMaking; set => yearOfMaking = value; }
        public string Manufacturer { get => manufacturer; set => manufacturer = value; }
        public string ModelVehicle { get => modelVehicle; set => modelVehicle = value; }
        public List<string> Service { get => service; set => service = value; }
        public string Type { get => type; set => type = value; }
        public string CustomerName { get => customerName; set => customerName = value; }

        public Vehicles()
        {
            
        }

        public string OilChange()
        {
            return "Change Oil";
        }

        public string EngineTuneup()
        {
            return "Engine Tuneup";
        }

        public string TransmissionCleanup()
        {
            return "Transmission Tuneup";
        }

        public string SpecificRequiredTask()
        {
            return "Specific Required Task";
        }
    }
}
