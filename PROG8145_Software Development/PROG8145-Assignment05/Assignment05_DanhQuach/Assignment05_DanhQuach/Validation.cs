﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;

namespace Assignment05_DanhQuach
{
    class RequiredValidation : ValidationRule
    {
        public override ValidationResult Validate(object value, CultureInfo cultureInfo)
        {
            if(value.ToString() == string.Empty || value == null)
            {
                return new ValidationResult(false, "Field is required");
            }
            else
            {
                return ValidationResult.ValidResult;
            }
        }
    }

    class YearValidation : ValidationRule
    {
        public override ValidationResult Validate(object value, CultureInfo cultureInfo)
        {
            int i = 0;
            if (!int.TryParse(value.ToString(), out i))
            {
                return new ValidationResult(false, "Year should be numberic");
            }
            else
            {
                if (i > DateTime.Now.Year)
                {
                    return new ValidationResult(false, "Year cannot greater than current year");
                }
                else if (i < 1900)
                {
                    return new ValidationResult(false, "Year cannot less than year 1900");
                }
                else
                {
                    return ValidationResult.ValidResult;
                }
            }
        }
    }
}
