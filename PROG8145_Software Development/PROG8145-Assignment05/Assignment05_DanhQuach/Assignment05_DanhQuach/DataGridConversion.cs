﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Data;
using System.Windows.Media;

namespace Assignment05_DanhQuach
{
    [ValueConversion(typeof(int), typeof(Brush))]
    class DataGridConversion : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            int i = int.Parse(value.ToString());
            int diff = DateTime.Now.Year - i;
            if (diff >= 10)
            {
                return Brushes.Red;
            }
            else
            {
                return Brushes.Transparent;
            }
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
