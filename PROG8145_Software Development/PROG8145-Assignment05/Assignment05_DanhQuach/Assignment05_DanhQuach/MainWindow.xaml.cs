﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Xml;
using System.Xml.Linq;
using System.Xml.Serialization;

namespace Assignment05_DanhQuach
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private ObservableCollection<Vehicle> datagrdVehicles = null;

        public ObservableCollection<Vehicle> DatagrdVehicles { get => datagrdVehicles; set => datagrdVehicles = value; }
        public MainWindow()
        {
            InitializeComponent();
            DatagrdVehicles = new ObservableCollection<Vehicle>();
            DataContext = this;
        }

        private void btnAdd_Click(object sender, RoutedEventArgs e)
        {
            Vehicle vehicle;
            switch (cbbType.SelectedValue.ToString())
            {
                case "Car":
                    vehicle = new Car();
                    break;
                case "Bus":
                    vehicle = new Bus();
                    break;
                case "Truck":
                    vehicle = new Truck();
                    break;
                case "Tractor":
                    vehicle = new Tractor();
                    break;
                default:
                    vehicle = new Vehicle();
                    break;
            }
            if (!Validation.GetHasError(txtYear) && !Validation.GetHasError(txtModel))
            {
                vehicle.Year = int.Parse(txtYear.Text);
                vehicle.Model = txtModel.Text;
                vehicle.Manufacturer = cbbMaker.SelectedValue.ToString();
                vehicle.Type = cbbType.SelectedValue.ToString();
                DatagrdVehicles.Add(vehicle);
            }
            dtgTransport.ItemsSource = DatagrdVehicles;
        }

        private void btnSaveXML_Click(object sender, RoutedEventArgs e)
        {
            string path = "vehicles.xml";
            try
            {
                ListVehicles list = new ListVehicles(DatagrdVehicles.ToList());
                XmlSerializer xmlSerializer = new XmlSerializer(typeof(ListVehicles));
                TextWriter writer = new StreamWriter(path);
                xmlSerializer.Serialize(writer, list);
                writer.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }
        }

        private void btnLoadXML_Click(object sender, RoutedEventArgs e)
        {
            DatagrdVehicles.Clear();
            ListVehicles list = null;
            string path = "vehicles.xml";
            try
            {
                if(File.Exists(path))
                {
                    XmlSerializer serializer = new XmlSerializer(typeof(ListVehicles));
                    StreamReader reader = new StreamReader(path);
                    list = (ListVehicles)serializer.Deserialize(reader);
                    reader.Close();
                    foreach (Vehicle v in list.listV)
                    {
                        DatagrdVehicles.Add(v);
                    }
                    lblInfo.Foreground = Brushes.Green;
                    lblInfo.Content = "XML file loaded successfully";
                }
                else
                {
                    lblInfo.Content = "File doesn't exist";
                }
            }
            catch (Exception ex)
            {
                lblInfo.Content = ex.InnerException;
            }
        }

        private void btnClearDataXML_Click(object sender, RoutedEventArgs e)
        {
            string path = "vehicles.xml";
            if (File.Exists(path))
            {
                File.Delete(path);
                lblInfo.Foreground = Brushes.Green;
                lblInfo.Content = "XML file deleted successfully";
            }
            else
            {
                lblInfo.Content = "File doesn't exist";
            }
        }

        private void btnFilter_Click(object sender, RoutedEventArgs e)
        {
            btnLoadXML_Click(sender, e);
            ListVehicles list = new ListVehicles();
            list.listV = DatagrdVehicles.ToList();
            if(cbbFilter.SelectedValue.ToString() == "All")
            {
                btnLoadXML_Click(sender, e);
            }
            else
            {
                var query = from vehicle in list.listV
                            where vehicle.GetType().Name == cbbFilter.SelectedValue.ToString()
                            select vehicle;
                DatagrdVehicles.Clear();
                foreach (Vehicle v in query)
                {
                    DatagrdVehicles.Add(v);
                }
            }
        }
    }
}
