﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace Assignment05_DanhQuach
{
    public class Vehicle
    {
        private int year;
        private string manufacturer;
        private string model;
        private string type;

        public Vehicle()
        {
            Year = 1900;
            Model = "AQ123";
        }

        public int Year { get => year; set => year = value; }
        public string Model { get => model; set => model = value; }
        public string Manufacturer { get => manufacturer; set => manufacturer = value; }
        public string Type { get => type; set => type = value; }
    }
    
    public class Car : Vehicle
    {
        public Car()
        {
        }
    }

    public class Truck : Vehicle
    {
        public Truck()
        {
        }
    }

    public class Bus : Vehicle
    {
        public Bus()
        {
        }
    }

    public class Tractor : Vehicle
    {
        public Tractor()
        {
        }
    }
    
    [XmlRoot("VehicleList") ]
    [XmlInclude(typeof(Car))]
    [XmlInclude(typeof(Bus))]
    [XmlInclude(typeof(Truck))]
    [XmlInclude(typeof(Tractor))]
    public class ListVehicles
    {
        [XmlArray("Vehicles")]
        [XmlArrayItem("Vehicle")]
        public List<Vehicle> listV;

        public ListVehicles()
        {
        }

        public ListVehicles(List<Vehicle> listV)
        {
            this.listV = listV;
        }
    }
}
