﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PROG8145_Assignment03
{
    class PickupTruck : Vehicle
    {
        public PickupTruck()
        {
            Task += RequiredTask;
            Task += EngineTuneup;
            Task += OilChange;
            Task += TransmissionCleanup;
        }

        public override void RequiredTask()
        {
            Console.WriteLine("Installation Cover");
        }
    }
}
