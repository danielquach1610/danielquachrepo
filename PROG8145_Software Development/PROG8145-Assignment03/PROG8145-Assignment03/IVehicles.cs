﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PROG8145_Assignment03
{
    interface IVehicles: IComparable
    {
        int YearOfMaking { get; set; }
        string Manufacturer { get; set; }
        string Model { get; set; }
        void OilChange();
        void EngineTuneup();
        void TransmissionCleanup();
        void RequiredTask();

    }
}
