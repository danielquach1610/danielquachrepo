﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PROG8145_Assignment03
{
    delegate void Task();
    abstract class Vehicle : IVehicles
    {
        private int yearOfMaking;
        private string manufacturer;
        private string model;
        private Task task = null;

        public Vehicle()
        {
        }

        public int YearOfMaking { get => yearOfMaking; set => yearOfMaking = value; }
        public string Manufacturer { get => manufacturer; set => manufacturer = value; }
        public string Model { get => model; set => model = value; }
        internal Task Task { get => task; set => task = value; }

        public void EngineTuneup()
        {
            Console.WriteLine("Engine Tuneup");
        }

        public void OilChange()
        {
            Console.WriteLine("Oil Change");
        }

        public void TransmissionCleanup()
        {
            Console.WriteLine("Transmission Cleanup");
        }

        public abstract void RequiredTask();

        public int CompareTo(object obj)
        {
            Vehicle v = (Vehicle)obj;
            return yearOfMaking.CompareTo(v.YearOfMaking);
        }
    }
}
