﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PROG8145_Assignment03
{
    class Tractor : Vehicle
    {
        public Tractor()
        {
            Task += RequiredTask;
            Task += EngineTuneup;
            Task += OilChange;
            Task += TransmissionCleanup;
        }

        public Tractor(int yearOfMaking, string manufacturer, string model) : base(yearOfMaking, manufacturer, model)
        {
            Task += RequiredTask;
            Task += EngineTuneup;
            Task += OilChange;
            Task += TransmissionCleanup;
        }

        public override void RequiredTask()
        {
            Console.WriteLine("PTO Maintenance");
        }
    }
}
