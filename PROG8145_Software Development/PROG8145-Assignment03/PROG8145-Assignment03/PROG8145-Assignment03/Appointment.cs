﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PROG8145_Assignment03
{
    class Appointment : IDisposable
    {

        List<Vehicle> vehicleList = null;
        public Appointment()
        {
            vehicleList = new List<Vehicle>();
        }

        public Vehicle this[int i]
        {
            get { return vehicleList[i]; }
            set { vehicleList[i] = value; }
        }

        public void Add(Vehicle vehicle)
        {
            vehicleList.Add(vehicle);
        }

        public int Count
        {
            get { return vehicleList.Count;}
        }

        public List<Vehicle> getList()
        {
            return vehicleList;
        }

        public void Dispose()
        {
            GC.SuppressFinalize(this);
        }
    }
}
