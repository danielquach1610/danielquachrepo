﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PROG8145_Assignment03
{
    class SchoolBus : Vehicle
    {
        public SchoolBus()
        {
            Task += RequiredTask;
            Task += EngineTuneup;
            Task += OilChange;
            Task += TransmissionCleanup;
        }

        public SchoolBus(int yearOfMaking, string manufacturer, string model) : base(yearOfMaking, manufacturer, model)
        {
            Task += RequiredTask;
            Task += EngineTuneup;
            Task += OilChange;
            Task += TransmissionCleanup;
        }

        public override void RequiredTask()
        {
            Console.WriteLine("Interior Cleanup");
        }
    }
}
