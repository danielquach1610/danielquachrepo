﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PROG8145_Assignment03
{
    class Program
    {
        static void Main(string[] args)
        {
            Program p = new Program();
            p.Execute();
            Console.ReadKey();
        }

        public void Execute()
        {
            string numberAppointmentString = string.Empty;
            int numberAppointment = 0;
            do
            {
                Console.Write("How many appointment: ");
                numberAppointmentString = Console.ReadLine();
            } while (!int.TryParse(numberAppointmentString, out numberAppointment) || numberAppointment < 0);
            if (numberAppointment == 0)
            {
                Console.WriteLine("No appointment");
            }
            else
            {
                MakingAppointment(numberAppointment);
            }
        }

        public void MakingAppointment(int numberAppointment)
        {
            using (Appointment appointment = new Appointment())
            {
                List<Vehicle> vlist = new List<Vehicle>();
                string typeString = string.Empty;
                int type = 0;

                for (int i = 0; i < numberAppointment; i++)
                {
                    Console.WriteLine("Customer {0}", i + 1);
                    do
                    {
                        Console.Write("Type: 1. Car / 2. School Bus / 3. Pickup Truck / 4. Tractor: ");
                        typeString = Console.ReadLine();
                    } while (!int.TryParse(typeString, out type) || type < 1 || type > 4);

                    Vehicle vehicle = null;
                    int year = getYearOfMaking();
                    string manufacturer = getManufacturer();
                    string model = getModel();

                    switch (type)
                    {
                        case 1:
                            vehicle = new Car(year, manufacturer, model);
                            appointment.Add(vehicle);
                            break;
                        case 2:
                            vehicle = new SchoolBus(year, manufacturer, model);
                            appointment.Add(vehicle);
                            break;
                        case 3:
                            vehicle = new PickupTruck(year, manufacturer, model);
                            appointment.Add(vehicle);
                            break;
                        case 4:
                            vehicle = new Tractor(year, manufacturer, model);
                            appointment.Add(vehicle);
                            break;
                        default:
                            break;
                    }
                    Console.WriteLine("***----------------***");
                }
                //Sorting the vehicle list
                vlist = appointment.getList();
                vlist.Sort();

                //Print out the Indexer appointment list
                Console.WriteLine("....................................................................");
                for (int i = 0; i < appointment.Count; i++)
                {
                    Console.WriteLine("{0}. {1}: {2} {3} {4};", i + 1, appointment[i].GetType().Name, appointment[i].YearOfMaking, appointment[i].Manufacturer, appointment[i].Model);
                    Console.WriteLine("Operation:");
                    appointment[i].Task();
                    Console.WriteLine("==========================");
                }
            }
        }

        public int getYearOfMaking()
        {
            //get info year of making
            string yearString = string.Empty;
            int yearOfMaking = 0;
            int currentYear = int.Parse(DateTime.Now.ToString("yyyy"));
            Console.WriteLine("------------------------------------------------------------------------");
            do
            {
                Console.Write("Manufactured Year (1900 <= Year <= current year): ");
                yearString = Console.ReadLine();
            } while (!int.TryParse(yearString, out yearOfMaking) || yearOfMaking < 1900 || yearOfMaking > currentYear);
            return yearOfMaking;
        }

        public string getManufacturer()
        {
            //get info Manufacturer
            string[] manufacturerStringList = { "Toyota", "Benz", "BMW", "Honda" };
            string manufacturerString = string.Empty;
            int manufacturerBrand = 0;
            Console.WriteLine("------------------------------------------------------------------------");
            do
            {
                for (int i = 0; i < manufacturerStringList.Length; i++)
                {
                    Console.Write("{0}. {1} / ", i, manufacturerStringList[i]);
                }
                Console.WriteLine("4. Other (Please specify)");
                Console.Write("Select manufacturer in list above by entering coresponding number: ");
                manufacturerString = Console.ReadLine();
            } while (!(int.TryParse(manufacturerString, out manufacturerBrand)) || (manufacturerBrand < 0) || (manufacturerBrand > 4));
            if (manufacturerBrand == 4)
            {
                do
                {
                    Console.Write("Specify other Manufacturer Name: ");
                    manufacturerString = Console.ReadLine();
                } while (manufacturerString == string.Empty);
            }
            else
            {
                manufacturerString = manufacturerStringList[manufacturerBrand];
            }
            return manufacturerString;
        }

        public string getModel()
        {
            string[] modelVehicleStringList = { "Skyline", "Hummel", "SUV", "Sedan" };
            string modelString = string.Empty;
            int model = 0;

            //get info Model
            Console.WriteLine("------------------------------------------------------------------------");
            do
            {
                for (int i = 0; i < modelVehicleStringList.Length; i++)
                {
                    Console.Write("{0}. {1} / ", i, modelVehicleStringList[i]);
                }
                Console.WriteLine("4. Other (Please specify)");
                Console.Write("Select model in list above by entering coresponding number: ");
                modelString = Console.ReadLine();
            } while (!(int.TryParse(modelString, out model)) || (model < 0) || (model > 4));
            if (model == 4)
            {
                do
                {
                    Console.Write("Specify other Model Name: ");
                    modelString = Console.ReadLine();
                } while (modelString == string.Empty);
            }
            else
            {
                modelString = modelVehicleStringList[model];
            }
            return modelString;
        }
    }
}
