﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PROG8145_Assignment04
{
    class Tractor : Vehicle
    {
        public Tractor()
        {
            Task = string.Concat(RequiredTask(), EngineTuneup(), OilChange(), TransmissionCleanup());
        }

        public override string RequiredTask()
        {
            return "PTO Maintenance;";
        }
    }
}
