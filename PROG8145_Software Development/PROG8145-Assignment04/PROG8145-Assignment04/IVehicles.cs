﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PROG8145_Assignment04
{
    interface IVehicles : IComparable
    {
        int YearOfMaking { get; set; }
        string Manufacturer { get; set; }
        string Model { get; set; }
        string OilChange();
        string EngineTuneup();
        string TransmissionCleanup();
        string RequiredTask();
    }
}
