﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace PROG8145_Assignment04
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public List<Vehicle> vlist = new List<Vehicle>();
        public MainWindow()
        {
            InitializeComponent();
            cbbVehicleType.SelectedIndex = 0;
            cbbManufacturer.SelectedIndex = 0;
            System.IO.File.WriteAllText(@"listVehicles.txt", string.Empty);
        }

        private void btnAdd_Click(object sender, RoutedEventArgs e)
        {
            bool validForm = false;
            validForm = validationInput(validForm);
            if (validForm == true)
            {
                FileStream fs = null;
                try
                {
                    fs = new FileStream("listVehicles.txt", FileMode.Append, FileAccess.Write);
                    BinaryWriter bw = new BinaryWriter(fs);
                    string vehicleType = cbbVehicleType.SelectedValue.ToString();
                    Vehicle vehicle = null;
                    switch (vehicleType)
                    {
                        case "Car":
                            vehicle = new Car();
                            break;
                        case "School Bus":
                            vehicle = new SchoolBus();
                            break;
                        case "Pickup Truck":
                            vehicle = new PickupTruck();
                            break;
                        case "Tractor":
                            vehicle = new Tractor();
                            break;
                        default:
                            break;
                    }
                    vehicle.YearOfMaking = int.Parse(txbYear.Text.ToString());
                    vehicle.Model = txbModel.Text;
                    vehicle.Manufacturer = cbbManufacturer.SelectedValue.ToString();
                    bw.Write(vehicle.YearOfMaking);
                    bw.Write(vehicle.GetType().Name);
                    bw.Write(vehicle.Manufacturer);
                    bw.Write(vehicle.Model);
                    bw.Write(vehicle.Task);
                    bw.Close();
                }
                catch (IOException ioe)
                {
                    Console.WriteLine(ioe.ToString());
                }
                finally
                {
                    fs.Close();
                }
                lblInformation.Content = "Appointment added";
                lblInformation.Foreground = Brushes.Green;
                txbYear.Text = string.Empty;
                txbModel.Text = string.Empty;
                cbbVehicleType.SelectedIndex = 0;
                cbbManufacturer.SelectedIndex = 0;
            }
        }

        public bool validationInput(bool validationFlag)
        {
            int temp = 0;
            int currentYear = int.Parse(DateTime.Now.ToString("yyyy"));
            if (txbYear.Text == string.Empty || !int.TryParse(txbYear.Text, out temp) || temp < 1900 || temp > currentYear)
            {
                txbYear.Background = Brushes.Red;
                txbYear.Focus();
                validationFlag = false;
            }
            else if (txbModel.Text == string.Empty)
            {
                txbModel.Background = Brushes.Red;
                txbModel.Focus();
                validationFlag = false;
            }
            else
            {
                validationFlag = true;
            }
            return validationFlag;
        }

        private void txbYear_TextChanged(object sender, TextChangedEventArgs e)
        {
            txbYear.Background = Brushes.White;
        }

        private void txbModel_TextChanged(object sender, TextChangedEventArgs e)
        {
            txbModel.Background = Brushes.White;
        }

        private void btnShow_Click(object sender, RoutedEventArgs e)
        {
            lblInformation.Content = "Show appointment list. Sorting by Year.";
            lblInformation.Foreground = Brushes.Blue;
            rtbAppList.Document.Blocks.Clear();
            vlist.Clear();
            FileStream fs = null;
            Vehicle vehicle = null;
            int year = 0;
            string type = string.Empty;
            string maker = string.Empty;
            string model = string.Empty;
            string tasks = string.Empty;
            try
            {
                fs = new FileStream("listVehicles.txt", FileMode.Open, FileAccess.Read);
                BinaryReader br = new BinaryReader(fs);
                while(br.BaseStream.Position != br.BaseStream.Length)
                {
                    year = br.ReadInt32();
                    type = br.ReadString();
                    maker = br.ReadString();
                    model = br.ReadString();
                    tasks = br.ReadString();
                    switch (type)
                    {
                        case "Car":
                            vehicle = new Car();
                            break;
                        case "SchoolBus":
                            vehicle = new SchoolBus();
                            break;
                        case "PickupTruck":
                            vehicle = new PickupTruck();
                            break;
                        case "Tractor":
                            vehicle = new Tractor();
                            break;
                        default:
                            break;
                    }
                    vehicle.YearOfMaking = year;
                    vehicle.Model = model;
                    vehicle.Manufacturer = maker;
                    vlist.Add(vehicle);
                    vlist.Sort();
                }
            }
            catch (IOException ioe)
            {
                //Console.WriteLine(ioe.ToString());
                lblInformation.Content = ioe.ToString();
            }
            finally
            {
                fs.Close();
            }
            for (int i = 0; i < vlist.Count; i++)
            {
                rtbAppList.AppendText(string.Format("Year: {0}; Type: {1}; Manufacturer: {2}; Model: {3}; Tasks: {4} \r",vlist[i].YearOfMaking, vlist[i].GetType().Name,vlist[i].Manufacturer,vlist[i].Model, vlist[i].Task));
            }
        }
    }
}
