﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PROG8145_Assignment04
{
    public abstract class Vehicle : IVehicles
    {
        private int yearOfMaking;
        private string manufacturer;
        private string model;
        private string task = string.Empty;

        public Vehicle()
        {
        }

        public int YearOfMaking { get => yearOfMaking; set => yearOfMaking = value; }
        public string Manufacturer { get => manufacturer; set => manufacturer = value; }
        public string Model { get => model; set => model = value; }
        internal string Task { get => task; set => task = value; }

        public string EngineTuneup()
        {
            return "Engine Tuneup;";
        }

        public string OilChange()
        {
            return "Oil Change;";
        }

        public string TransmissionCleanup()
        {
            return "Transmission Cleanup;";
        }

        public abstract string RequiredTask();

        public int CompareTo(object obj)
        {
            Vehicle v = (Vehicle)obj;
            return yearOfMaking.CompareTo(v.YearOfMaking);
        }
    }
}
