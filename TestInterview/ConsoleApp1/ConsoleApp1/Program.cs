﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1
{
    class Program
    {
        static void Main(string[] args)
        {
            Program p = new Program();

            for (int i = 1; i <= 3; i++)
            {
                string filename = "input00" + i + ".txt";
                //string output = p.Validate2("input002.txt");
                string output = p.Validate2(filename);
                Console.WriteLine(filename + " : " + output);
            }
            Console.ReadKey();
        }

        private string Validate2(string filename)
        {
            string line1 = File.ReadLines(filename).First();
            var total = int.Parse(line1);
            var logFile = File.ReadAllLines(filename);
            var logList = new List<string>(logFile);
            var poseAfterOrient = 0;
            var orientAfterArm = 0;
            var connectAfterPair = 0;
            var armAfterConnect = 0;
            var armBeforeDis = 0;
            string temp = string.Empty;

            //validate timestamp
            if (!validateTimestamp(logList, total)) return "bad";

            //validate event order
            for (int i = 1; i <= total; i++)
            {
                string[] strArr = logList[i].Split(',');
                //check if event is paired
                if (strArr[3].Equals("paired"))
                {
                    //check if event is connected
                    if (logList[i + 1].Split(',')[3].Equals("connected"))
                    {
                        connectAfterPair = 1;
                        for (int j = i + 2; j <= total; j++)
                        {
                            //check if event is arm_synced
                            if (logList[j].Split(',')[3].Equals("arm_synced"))
                            {
                                armAfterConnect = 1;
                                for (int k = j + 1; k <= total; k++)
                                {
                                    temp = logList[k].Split(',')[3];
                                    //check if event is orientation
                                    if (logList[k].Split(',')[3].Equals("orientation"))
                                    {
                                        orientAfterArm = 1;
                                        for (int m = k + 1; m <= total; m++)
                                        {
                                            temp = logList[m].Split(',')[3];
                                            ////check if event is pose
                                            if (logList[m].Split(',')[3].Equals("pose"))
                                                poseAfterOrient = 1;
                                            if (temp.Equals("arm_unsynced")) break;
                                        }
                                    }
                                    if (temp.Equals("arm_unsynced")) break;
                                }
                            }
                            //if (temp.Equals("disconnected")) break;
                        }
                    }
                }
                if (strArr[3].Equals("unpaired"))
                {
                    if (!logList[i - 1].Split(',')[3].Equals("disconnected")) return "bad";
                    else
                    {
                        for (int j = i - 2; j > 0; j--)
                        {
                            if (logList[j].Split(',')[3].Equals("arm_unsynced"))
                            {
                                armBeforeDis = 1;
                                break;
                            }
                        }
                    }
                }
            }
            if (poseAfterOrient == 0 || orientAfterArm == 0 || connectAfterPair == 0 || armAfterConnect == 0
                || armBeforeDis == 0)
                return "bad";
            return "good";
        }

        private Boolean validateTimestamp(List<string> logList, int total)
        {
            for (int i = 1; i < total; i++)
            {
                string[] strArr1 = logList[i].Split(',');
                long curtp = long.Parse(strArr1[1]);
                for (int j = i + 1; j <= total; j++)
                {

                    string[] strArr2 = logList[j].Split(',');
                    long nexttp = long.Parse(strArr2[1]);
                    string nexttype = strArr2[3];
                    if (nexttp < curtp) return false;
                }
            }
            return true;
        }
    }
}