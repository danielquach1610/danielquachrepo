document.addEventListener('keydown', function (event) {
	var moveBox = 40;
    if (event.keyCode == 37) {
        console.log("Left");
        mario.position.x -= moveBox;
    }
    else if (event.keyCode == 39) {
        console.log("Right");
        mario.position.x += moveBox;
    }
    else if (event.keyCode == 38) {
        console.log("Up");
        mario.position.y -= moveBox;
    }
    else if (event.keyCode == 40) {
        console.log("Down");
        mario.position.y += moveBox;
    }
});