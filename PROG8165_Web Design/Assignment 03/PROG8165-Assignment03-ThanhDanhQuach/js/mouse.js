document.addEventListener('mousedown', function (event) {
	var moveBox = 40;
	if (event.button == 0)
	{
		var x = event.clientX;
		var y = event.clientY;
        console.log("Click X:  " + x + "   Y: " + y);
        //Move the coordinator from top left corner to center of texture
        var lastX = mario.position.x + moveBox/2;
        var lastY = mario.position.y + moveBox/2;
		console.log("Last X :  " + lastX + "   Y: " + lastY);
		var deltaX = lastX - x;
        var deltaY = lastY - y;
        //Calculate the different between new & old coordinator
		console.log("delta X:  " + deltaX + "   Y: " + deltaY);
        //Check direction of mouse click
        if (Math.abs(deltaX) > Math.abs(deltaY) && deltaX > 0 && Math.abs(deltaX) > moveBox/2) {
            console.log("Left"); 
            mario.position.x -= moveBox;
        } else if (Math.abs(deltaX) > Math.abs(deltaY) && deltaX < 0 && Math.abs(deltaX) > moveBox / 2) {
            console.log("Right");
            mario.position.x += moveBox;
        } else if (Math.abs(deltaY) > Math.abs(deltaX) && deltaY > 0 && Math.abs(deltaY) > moveBox / 2) {
            console.log("Up");
            mario.position.y -= moveBox;
        } else if (Math.abs(deltaY) > Math.abs(deltaX) && deltaY < 0 && Math.abs(deltaY) > moveBox / 2) {
            console.log("Down");
            mario.position.y += moveBox;
        }
		console.log("---------------------------------------------------------");
	}
});