function validateName()
{
	//validate Name required
	var name = document.getElementById("name");
	if (name.value == '')
	{
		name.setCustomValidity('Name is required');
	}
	else
	{
		name.setCustomValidity('');
	}

}

function validateEmail()	
{
	//validate Email required and follow pattern
	var email = document.getElementById("mail");
	if (email.value == '')
	{
		email.setCustomValidity('Email is required');
	}
	else
	{
		email.setCustomValidity('');
	}
}

function validatePassword()
{
	//validate password and confirm password required and matched
	var pw2 = document.getElementById("pass2");
	var pw1 = document.getElementById("pass1");
	if (pw1.value == '')
	{
		pw1.setCustomValidity('Password is required');
	}
	else
	{
		pw1.setCustomValidity('');
	}
	if (pw2.value == '')
	{
		pw2.setCustomValidity('Confirm Password is required');
	}
	else
	{
		if (pw2.value != pw1.value)
		{
			pw2.setCustomValidity('Confirm Password does not match');
		}
		else
		{
			pw2.style.backgroundColor = "lightgreen";
			pw2.setCustomValidity('');
		}
	}
	
}

