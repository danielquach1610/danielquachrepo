function swapStyleSheet(sheet){
	document.getElementById('pagestyle').setAttribute('href', sheet);
	setCookie(sheet);
}

function setCookie(sheet) {
    document.cookie = sheet;
}

function getCookie() {
    var decodedCookie = decodeURIComponent(document.cookie);
    return decodedCookie;
}

function checkCookieAndSetStyleSheet(){
	var sheet = getCookie();
	if (sheet != "") {
		swapStyleSheet(sheet);
	}
}