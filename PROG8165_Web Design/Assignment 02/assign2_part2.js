var slideimages = new Array();
var slidelinks = new Array();

function slideshowimages() {
	for (i = 0; i < slideshowimages.arguments.length; i++) {
		slideimages[i] = new Image();
		slideimages[i].src = slideshowimages.arguments[i];
	}
}

function slideshowlinks() {
	for (i = 0; i < slideshowlinks.arguments.length; i++)
		slidelinks[i] = slideshowlinks.arguments[i];
}

function gotoshow() {
	if (!window.winslide || winslide.closed)
		winslide = window.open(slidelinks[whichlink]);
	else
		winslide.location = slidelinks[whichlink];
	winslide.focus();
}
//TO DO: Configure the paths of the images, plus corresponding target links 
slideshowimages("img/London.jpg","img/Paris.jpg","img/Saigon.jpg","img/Singapore.jpg","img/Toronto.jpg");
//TO DO: Configure image target links 
slideshowlinks("https://en.wikipedia.org/wiki/London","https://en.wikipedia.org/wiki/Paris","https://en.wikipedia.org/wiki/Saigon","https://en.wikipedia.org/wiki/Singapore","https://en.wikipedia.org/wiki/Toronto");
//TO DO: configure the speed of the slideshow, in milliseconds
var slideshowspeed = 2000;
var whichlink = 0;
var whichimage = 0;

function slideit() {
	if (!document.images)
		return;
	document.images.slide.src = slideimages[whichimage].src;
	whichlink = whichimage;
	if (whichimage < slideimages.length - 1)
		whichimage++;
	else
		whichimage = 0;
	setTimeout("slideit()", slideshowspeed);
}