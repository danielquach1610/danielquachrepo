function changePara1() {
	document.getElementById("para1").innerHTML = "Paragrah 1:New Text!";
}

function changePara1Back() {
	document.getElementById("para1").innerHTML = "Paragrah 1: Original Text";
}

function insertCurrentDate() {
	document.getElementById("datePara").innerHTML = Date();
}

function hoverCSS() {
	var myElement = document.querySelector("#para1");
	myElement.style.backgroundColor = "#8db5fc";
	myElement.style.color = "#ed3d46";
}

function outCSS() {
	var myElement = document.querySelector("#para1");
	myElement.style.backgroundColor = "#ffffff";
	myElement.style.color = "#000000";
}