﻿using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Firefox;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Assignment04_QA;

namespace Assignment04_Selenium
{
    [TestFixture]
    public class JDPowerTest
    {
        private IWebDriver driver;
        private StringBuilder verificationErrors;
        private string baseURL;
        private bool acceptNextAlert = true;

        [SetUp]
        public void SetupTest()
        {
            driver = new ChromeDriver();
            baseURL = "http://localhost:57832/Vehicle";
            verificationErrors = new StringBuilder();
        }

        [TearDown]
        public void TeardownTest()
        {
            try
            {
                driver.Quit();
            }
            catch (Exception)
            {
                // Ignore errors if unable to close the browser
            }
            Assert.AreEqual("", verificationErrors.ToString());
        }

        [TestCase("Jonas","900 Old Cartrige","Kitchener","123-334-5567","jonas@mail.com","Acura","RLX",2018)]
        [TestCase("Lucas", "321 Doon", "Kitchener", "123-334-9875", "lucas@mail.com", "Audi", "Q5", 2018)]
        [TestCase("Ronald", "500 King St", "Toronto", "(332)456-3312", "ronald@dmail.com", "Chrysler", "Pacifia", 2018)]
        public void Registration_VerifySellerContactInformation_AllInformationCorrect
            (String name, String address, String city,String phone,
            String email, String make, String model, short year)
        {
            Assignment04_QA.Models.Vehicle vehicle = new Assignment04_QA.Models.Vehicle();
            vehicle.sellerName = name;
            vehicle.address = address;
            vehicle.city = city;
            vehicle.phone = phone;
            vehicle.email = email;
            vehicle.make = make;
            vehicle.model = model;
            vehicle.year = year;

            driver.Navigate().GoToUrl("http://localhost:57832/Vehicle");
            driver.FindElement(By.LinkText("New")).Click();
            driver.FindElement(By.Name("sellerName")).Clear();
            driver.FindElement(By.Name("sellerName")).SendKeys(vehicle.sellerName);
            driver.FindElement(By.Name("address")).Clear();
            driver.FindElement(By.Name("address")).SendKeys(vehicle.address);
            driver.FindElement(By.Name("city")).Clear();
            driver.FindElement(By.Name("city")).SendKeys(vehicle.city);
            driver.FindElement(By.Name("phone")).Clear();
            driver.FindElement(By.Name("phone")).SendKeys(vehicle.phone);
            driver.FindElement(By.Name("email")).Clear();
            driver.FindElement(By.Name("email")).SendKeys(vehicle.email);
            driver.FindElement(By.Name("make")).Clear();
            driver.FindElement(By.Name("make")).SendKeys(vehicle.make);
            driver.FindElement(By.Name("model")).Clear();
            driver.FindElement(By.Name("model")).SendKeys(vehicle.model);
            driver.FindElement(By.Name("year")).Clear();
            driver.FindElement(By.Name("year")).SendKeys(vehicle.year.ToString());
            driver.FindElement(By.XPath("//input[@value='Submit']")).Click();
            Assert.IsTrue(driver.FindElement(By.XPath("//DD[text()='"+ vehicle.sellerName +"']")).Displayed);
            Assert.IsTrue(driver.FindElement(By.XPath("//DD[text()='" + vehicle.address + "']")).Displayed);
            Assert.IsTrue(driver.FindElement(By.XPath("//DD[text()='" + vehicle.city + "']")).Displayed);
            Assert.IsTrue(driver.FindElement(By.XPath("//DD[text()='" + vehicle.phone + "']")).Displayed);
            Assert.IsTrue(driver.FindElement(By.XPath("//DD[text()='" + vehicle.email + "']")).Displayed);
            Assert.IsTrue(driver.FindElement(By.XPath("//DD[text()='" + vehicle.make + "']")).Displayed);
            Assert.IsTrue(driver.FindElement(By.XPath("//DD[text()='" + vehicle.model + "']")).Displayed);
            Assert.IsTrue(driver.FindElement(By.XPath("//DD[text()='" + vehicle.year + "']")).Displayed);
            Assert.IsTrue(driver.FindElement(By.XPath("//A[@href='http://www.jdpower.com/cars/"
                + vehicle.make + "/"
                + vehicle.model + "/"
                + vehicle.year +"'][text()='http://www.jdpower.com/cars/"
                + vehicle.make +"/"
                + vehicle.model +"/"
                + vehicle.year +"']")).Displayed);
        }

        [TestCase("Honda","Aruna",2015)]
        [TestCase("BMW", "Z4", 2004)]
        [TestCase("Chevrolet", "Corvette", 2017)]
        public void Registration_VerifyLinkToCorrectPage_JDPowerPageIsOpenWithCorrectURL(String make, String model, int year)
        {
            String url = "http://www.jdpower.com/cars/" + make + "/" + model + "/" + year.ToString();
            driver.Navigate().GoToUrl("http://localhost:57832/Vehicle");
            driver.FindElement(By.LinkText("Search")).Click();
            driver.FindElement(By.LinkText(url)).Click();
            Assert.AreEqual(driver.Url,url);
        }

        //[TestCase("Lucas", "321 Doon", "Kitchener", "123-334-9875", "lucas@mail.com", "Acura", "RLX", 2018)]
        [TestCase("", "321 Doon", "Kitchener", "123-334-9875", "lucas@mail.com", "Acura", "RLX", 2018)]
        [TestCase("Lucas", "", "Kitchener", "123-334-9875", "lucas@mail.com", "Acura", "RLX", 2018)]
        [TestCase("Lucas", "321 Doon", "", "123-334-9875", "lucas@mail.com", "Acura", "RLX", 2018)]
        [TestCase("Lucas", "321 Doon", "Kitchener", "", "lucas@mail.com", "Acura", "RLX", 2018)]
        [TestCase("Lucas", "321 Doon", "Kitchener", "123-334-9875", "", "Acura", "RLX", 2018)]
        [TestCase("Lucas", "321 Doon", "Kitchener", "123-334-9875", "lucas@mail.com", "", "RLX", 2018)]
        [TestCase("Lucas", "321 Doon", "Kitchener", "123-334-9875", "lucas@mail.com", "Acura", "", 2018)]
        [TestCase("Lucas", "321 Doon", "Kitchener", "123-334-9875", "lucas@mail.com", "Acura", "RLX",0)]
        public void Registration_VerifyAllFieldsAreMandatory_ErrorMsgDisplay
            (String name, String address, String city, String phone,
            String email, String make, String model, short year)
        {
            string yearStr = string.Empty;
            if (year != 0) yearStr = year.ToString();
            driver.Navigate().GoToUrl("http://localhost:57832/Vehicle");
            driver.FindElement(By.LinkText("New")).Click();
            driver.FindElement(By.Name("sellerName")).Clear();
            driver.FindElement(By.Name("sellerName")).SendKeys(name);
            driver.FindElement(By.Name("address")).Clear();
            driver.FindElement(By.Name("address")).SendKeys(address);
            driver.FindElement(By.Name("city")).Clear();
            driver.FindElement(By.Name("city")).SendKeys(city);
            driver.FindElement(By.Name("phone")).Clear();
            driver.FindElement(By.Name("phone")).SendKeys(phone);
            driver.FindElement(By.Name("email")).Clear();
            driver.FindElement(By.Name("email")).SendKeys(email);
            driver.FindElement(By.Name("make")).Clear();
            driver.FindElement(By.Name("make")).SendKeys(make);
            driver.FindElement(By.Name("model")).Clear();
            driver.FindElement(By.Name("model")).SendKeys(model);
            driver.FindElement(By.Name("year")).Clear();
            driver.FindElement(By.Name("year")).SendKeys(yearStr);
            driver.FindElement(By.XPath("//input[@value='Submit']")).Click();
            string errMsg = driver.FindElement(By.Id("errorMsg")).Text;
            Assert.AreEqual("All fields are required", errMsg);
        }

        [TestCase("Lucas", "321 Doon", "Kitchener", "1233349875", "lucas@mail.com", "Acura", "RLX", 2018)]
        [TestCase("Lucas", "321 Doon", "Kitchener", "123---334-987545", "lucas@mail.com", "Acura", "RLX", 2018)]
        public void Registration_VerifyInputInvalidPhoneNumber_ErrorMsgDisplay
            (String name, String address, String city, String phone,
            String email, String make, String model, short year)
        {
            string yearStr = string.Empty;
            if (year != 0) yearStr = year.ToString();
            driver.Navigate().GoToUrl("http://localhost:57832/Vehicle");
            driver.FindElement(By.LinkText("New")).Click();
            driver.FindElement(By.Name("sellerName")).Clear();
            driver.FindElement(By.Name("sellerName")).SendKeys(name);
            driver.FindElement(By.Name("address")).Clear();
            driver.FindElement(By.Name("address")).SendKeys(address);
            driver.FindElement(By.Name("city")).Clear();
            driver.FindElement(By.Name("city")).SendKeys(city);
            driver.FindElement(By.Name("phone")).Clear();
            driver.FindElement(By.Name("phone")).SendKeys(phone);
            driver.FindElement(By.Name("email")).Clear();
            driver.FindElement(By.Name("email")).SendKeys(email);
            driver.FindElement(By.Name("make")).Clear();
            driver.FindElement(By.Name("make")).SendKeys(make);
            driver.FindElement(By.Name("model")).Clear();
            driver.FindElement(By.Name("model")).SendKeys(model);
            driver.FindElement(By.Name("year")).Clear();
            driver.FindElement(By.Name("year")).SendKeys(yearStr);
            driver.FindElement(By.XPath("//input[@value='Submit']")).Click();
            string errMsg = driver.FindElement(By.Id("errorMsg")).Text;
            Assert.AreEqual("Phone Number is invalid. Should be 123-123-1234 or (123)123-1234", errMsg);
        }

        [TestCase(3000)]
        [TestCase(1234)]
        public void Registration_VerifyInputInvalidYear_ErrorMsgOnTooltip(short year)
        {
            string yearStr = string.Empty;
            string expectStr = string.Empty;
            if (year != 0)
            {
                if(year > 2018) expectStr = "Value must be less than or equal to 2018.";
                if (year < 1900) expectStr = "Value must be greater than or equal to 1900.";
                yearStr = year.ToString();
            }
            driver.Navigate().GoToUrl("http://localhost:57832/Vehicle");
            driver.FindElement(By.LinkText("New")).Click();
            driver.FindElement(By.Name("year")).Clear();
            driver.FindElement(By.Name("year")).SendKeys(yearStr);
            IJavaScriptExecutor js = (IJavaScriptExecutor)driver;
            IWebElement field = driver.FindElement(By.Name("year"));
            Boolean is_valid = (Boolean)js.ExecuteScript("return arguments[0].checkValidity();", field);
            String message = (String)js.ExecuteScript("return arguments[0].validationMessage;", field);
            Assert.AreEqual(expectStr, message);
        }

        [TestCase("EmailWithoutExtension@")]
        [TestCase("EmailWithoutAtSign")]
        public void Registration_VerifyInputInvalidEmail_ErrMsgOnTooltip(String email)
        {
            string expectStr = string.Empty;
            if (email.Contains("@"))
                expectStr = "Please enter a part following '@'. '" + email + "' is incomplete.";
            else
                expectStr = "Please include an '@' in the email address. '" + email + "' is missing an '@'.";
            driver.Navigate().GoToUrl("http://localhost:57832/Vehicle");
            driver.FindElement(By.LinkText("New")).Click();
            driver.FindElement(By.Name("email")).Clear();
            driver.FindElement(By.Name("email")).SendKeys(email);
            driver.FindElement(By.XPath("//input[@value='Submit']")).Click();
            IJavaScriptExecutor js = (IJavaScriptExecutor)driver;
            IWebElement field = driver.FindElement(By.Name("email"));
            Boolean is_valid = (Boolean)js.ExecuteScript("return arguments[0].checkValidity();", field);
            String message = (String)js.ExecuteScript("return arguments[0].validationMessage;", field);
            Assert.AreEqual(expectStr, message);
        }
        private bool IsElementPresent(By by)
        {
            try
            {
                driver.FindElement(by);
                return true;
            }
            catch (NoSuchElementException)
            {
                return false;
            }
        }

        private bool IsAlertPresent()
        {
            try
            {
                driver.SwitchTo().Alert();
                return true;
            }
            catch (NoAlertPresentException)
            {
                return false;
            }
        }

        private string CloseAlertAndGetItsText()
        {
            try
            {
                IAlert alert = driver.SwitchTo().Alert();
                string alertText = alert.Text;
                if (acceptNextAlert)
                {
                    alert.Accept();
                }
                else
                {
                    alert.Dismiss();
                }
                return alertText;
            }
            finally
            {
                acceptNextAlert = true;
            }
        }
    }
}
