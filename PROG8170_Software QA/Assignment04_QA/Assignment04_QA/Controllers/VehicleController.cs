﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;

namespace Assignment04_QA.Controllers
{
    public class VehicleController : Controller
    {
        private Models.jdpowerEntities db = new Models.jdpowerEntities();
        // GET: Vehicle
        public ActionResult Index()
        {
            return View();
        }

        // GET : List
        public ActionResult List()
        {
            return View(db.Vehicles.ToList());
        }

        // GET: Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Create
        public ActionResult CreateNew([Bind(Include = "Id,sellerName,address,city,phone,email,make,model,year")] Models.Vehicle vehicle)
        {
            if(ModelState.IsValid)
            {
                db.Vehicles.Add(vehicle);
                db.SaveChanges();
                return RedirectToAction("Detail", new { id = vehicle.Id});
            }
            return View();
        }

        // GET : Vehicle/Detail
        public ActionResult Detail(int? id)
        {
            if (id == null) return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            Models.Vehicle vehicle = db.Vehicles.Find(id);
            if (vehicle == null) return HttpNotFound();
            return View(vehicle);
        }

        // GET : Vehicle/Delete
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Models.Vehicle vehicle = db.Vehicles.Find(id);
            if (vehicle == null)
            {
                return HttpNotFound();
            }
            else
            {
                db.Vehicles.Remove(vehicle);
                db.SaveChanges();
                return RedirectToAction("List");
            }
        }
    }
}