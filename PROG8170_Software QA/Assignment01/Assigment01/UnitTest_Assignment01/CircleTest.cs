﻿using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Assigment01;

namespace UnitTest_Assignment01
{
    [TestFixture]
    public class CircleTest
    {
        Circle c;

        [SetUp]
        public void Init()
        {
            c = new Circle();
        }

        [Test]
        public void Constructor_InstantiateWithRadius_ExpectedEqual()
        {
            c = new Circle(5.2);
            Assert.AreEqual(5.2, c.radius);
        }

        [Test]
        public void Method_AddToRadiusWithIntegerNumber_ExpectedEqual()
        {
            c.radius = 5.4;
            int number = 6;
            double total = 5.4 + 6;
            c.AddToRadius(number);
            Assert.AreEqual(total, c.radius);
        }

        [Test]
        public void Method_AddToRadiusWithNegativeDoubleNumberSmallerThanRadius_ExpectedEqual()
        {
            c.radius = 6.5;
            double number = -5.56;
            double total = 6.5 - 5.56;
            c.AddToRadius(number);
            Assert.AreEqual(total, c.radius);
        }

        [Test]
        public void Method_SubstractFromRadiusWithIntNumberSmallerThanRadius_ExpectedEqual()
        {
            c.radius = 12.64;
            int number = 6;
            double total = 12.64 - 6;
            c.SubsctractFromRadius(number);
            Assert.AreEqual(total, c.radius);
        }

        [Test]
        [ExpectedException(typeof(NegativeRadiusException))]
        public void Method_SubstractFromRadiusWithDoubleNumberLargerThanRadius_ExpectedException()
        {
            c.radius = 7.64;
            double number = 8.5;
            c.SubsctractFromRadius(number);
        }

        [Test]
        public void Method_CalculateCircumferenceWithPositiveRadius_ExpectedEqual()
        {
            c.radius = 5.7;
            double c1 = 2* 5.7 * Math.PI;
            double c2 = c.GetCircumference();
            Assert.AreEqual(c1, c2);
        }

        [Test]
        [ExpectedException(typeof(NegativeRadiusException))]
        public void Method_CalculateCircumferenceWithNegativeRadius_ExpectedException()
        {
            c.radius = -5.7;
            double c2 = c.GetCircumference();
        }

        [Test]
        public void Method_CalculateAreaWithPositiveRadius_ExpectedEqual()
        {
            c.radius = 5.7;
            double c1 = 5.7 * 5.7 * Math.PI;
            double c2 = c.GetArea();
            Assert.AreEqual(c1, c2);
        }

        [Test]
        public void Method_CalculateAreaWithNegativeRadius_ExpectedEqual()
        {
            c.radius = -5.7;
            double c1 = -5.7 * -5.7 * Math.PI;
            double c2 = c.GetArea();
            Assert.AreEqual(c1, c2);
        }
    }
}