﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

/// <summary>
/// Assigment01 by Danh Quach Thanh - 7830615
/// </summary>
namespace Assigment01
{
    class Program
    {
        static void Main(string[] args)
        {
            Program p = new Program();
            while(1==1)
            {
                p.Go();
            };
        }

        private void Go()
        {
            Circle c = new Circle();
            string radiusString = string.Empty;
            double circumference = 0;
            double area = 0;
            do
            {
                Console.Write("Enter radius (>=0): ");
                radiusString = Console.ReadLine();
            } while (!double.TryParse(radiusString, out c.radius) || c.radius < 0);

            string selectionString = string.Empty;
            int selection = 0;
            do
            {
                Console.WriteLine("1. Add to Circle Radius");
                Console.WriteLine("2. Substract from Circle Radius");
                Console.WriteLine("3. Calculate Circle Circumference");
                Console.WriteLine("4. Calculate Circle Area");
                Console.WriteLine("5. Exit");
                Console.Write("Enter choice: ");
                selectionString = Console.ReadLine();
            } while (!int.TryParse(selectionString, out selection) || selection < 1 || selection > 5);

            double calculateNumber = 0;
            switch(selection)
            {
                case 1:
                    calculateNumber = ValidateNumberInput();
                    c.AddToRadius(calculateNumber);
                    Console.Write("New Radius: {0}", c.radius);
                    Console.WriteLine();
                    break;
                case 2:
                    calculateNumber = ValidateNumberInput();
                    c.SubsctractFromRadius(calculateNumber);
                    Console.Write("New Radius: {0}", c.radius);
                    Console.WriteLine();
                    break;
                case 3:
                    circumference = c.GetCircumference();
                    Console.Write("Cirlce Circumference: {0}", circumference);
                    Console.WriteLine();
                    break;
                case 4:
                    area = c.GetArea();
                    Console.Write("Circle Area: {0}", area);
                    Console.WriteLine();
                    break;
                case 5:
                    Environment.Exit(0);
                    break;
                default:
                    break;
            }
            Console.WriteLine("--------------------------------------------------------");
        }

        private double ValidateNumberInput()
        {
            string numberString = string.Empty;
            double number = 0;
            do
            {
                Console.Write("Enter a number: ");
                numberString = Console.ReadLine();
            } while (!double.TryParse(numberString, out number));

            return number;
        }
    }
}