﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assigment01
{
    public class Circle
    {
        public double radius;

        public Circle()
        {

        }

        public Circle(double radius)
        {
            this.radius = radius;
        }

        public void AddToRadius(double num)
        {
            double total = radius + num;
            if (total < 0)
            {
                throw new NegativeRadiusException(string.Format("Radius cannot go below zero"));
            }
            else
                radius = total;
        }

        public void SubsctractFromRadius(double num)
        {
            double total = radius - num;
            if (total < 0)
            {
                throw new NegativeRadiusException(string.Format("Exception: Radius cannot go below zero"));
            }
            else
                radius = total;
        }

        public double GetCircumference()
        {
            if (radius < 0)
                throw new NegativeRadiusException(string.Format("Exception: Radius cannot go below zero"));
            else
            {
                var c = 2 * radius * Math.PI;
                return c;
            }
        }

        public double GetArea()
        {
            var a = radius * radius * Math.PI;
            return a;
        }
    }

    public class NegativeRadiusException : Exception
    {
        public NegativeRadiusException(string message) : base(message) { }
    }
}