﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using GMap.NET;
using GMap.NET.WindowsForms;
using GMap.NET.WindowsForms.Markers;
using GMap.NET.MapProviders;
using Parse;

namespace Lab06_HQ
{
    public partial class Form1 : Form
    {
        GMapOverlay markerOverlay = new GMapOverlay();

        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            ParseClient.Initialize(new ParseClient.Configuration
            {
                ApplicationId = "Z0Xb7S69YzOzEZab4ZScsmyuUIdKuedkxJtcdNYn",
                WindowsKey = "dKNkvdeILwsmOyDBbKYed8WfQox0EHtHnMhuyP8r",

                // the serverURL of your hosted Parse Server
                Server = "https://parseapi.back4app.com/"
            });

            gMapControl1.SetBounds(0, 0, ClientRectangle.Width, ClientRectangle.Height);
            pictureBox1.SetBounds(0, 0, ClientRectangle.Width, ClientRectangle.Height);
            
            //Init map & zoom information
            gMapControl1.MapProvider = GMap.NET.MapProviders.GoogleHybridMapProvider.Instance;
            GMaps.Instance.Mode = AccessMode.ServerOnly;
            gMapControl1.GrayScaleMode = true;
            gMapControl1.MinZoom = 1;
            gMapControl1.MaxZoom = 20;
            gMapControl1.Zoom = 16;

            pictureBox1.Parent = gMapControl1;
            pictureBox1.Enabled = false;
            pictureBox2.Parent = gMapControl1;

            gMapControl1.Position = new PointLatLng(43.388970, -80.404852);
            gMapControl1.Overlays.Add(markerOverlay);

            timer1.Start();
        }

        private void pictureBox2_Click(object sender, EventArgs e)
        {
            var r = new Random();
            double randomLat = 43.388970 + 0.0001 * r.Next(-100, 100);
            double randomLon = -80.404852 + 0.0001 * r.Next(-100, 100);

            GMarkerGoogle gMarker = new GMarkerGoogle(new PointLatLng(randomLat, randomLon), GMarkerGoogleType.red_small);
            markerOverlay.Markers.Add(gMarker);

            //refresh
            gMapControl1.Invalidate();
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            var query = ParseObject.GetQuery("GPSTrack");

            IEnumerable<ParseObject> results = query.FindAsync().Result;
            foreach(var item in results)
            {
                //make marker
                string coor = item["location"].ToString();

                //tokenize base on,
                string[] words = coor.Split(',');
                //word[0] = lat; word[1] = lon;
                double lat = double.Parse(words[0]);
                double lon = double.Parse(words[1]);

                GMarkerGoogle gMarker = new GMarkerGoogle(new PointLatLng(lat, lon), GMarkerGoogleType.red_small);
                markerOverlay.Markers.Add(gMarker);

                //refresh
                gMapControl1.Invalidate();
            }
        }
    }
}
