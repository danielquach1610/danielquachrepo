package com.back4app.quickstartexampleapp;

import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.parse.Parse;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseUser;
import com.parse.ParseInstallation;

import java.text.DateFormat;
import java.util.Date;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // Save the current Installation to Back4App
        //ParseInstallation.getCurrentInstallation().saveInBackground();

        // Launch 3 second timer
        timerHandler.postDelayed(timerRunnable, 0);
    }

    Handler timerHandler = new Handler();


    Runnable timerRunnable = new Runnable() {
        @Override
        public void run() {
            ParseObject gpsTrack = new ParseObject("GPSTrack"); // instantiate new object
            gpsTrack.put("lat", "43.396815");
            gpsTrack.put("lon", "-80.3992645,15");
            gpsTrack.saveInBackground();
            timerHandler.postDelayed(this, 3000);
        }
    };
}
