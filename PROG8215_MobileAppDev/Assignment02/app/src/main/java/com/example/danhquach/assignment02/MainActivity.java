package com.example.danhquach.assignment02;

import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.provider.MediaStore;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        capturePhoto(null);
    }

    public void capturePhoto(View v) {
        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        if (takePictureIntent.resolveActivity(getPackageManager()) != null) {
            startActivityForResult(takePictureIntent, 42);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == 42 && resultCode == RESULT_OK) {
            Bundle extras = data.getExtras();
            Bitmap imageBitmap = (Bitmap) extras.get("data");
            DrawImageView mImageView = (DrawImageView) findViewById(R.id.drawImageView2);
            EditText editText = (EditText) findViewById(R.id.txtComment);
            mImageView.inputText = editText.getText().toString();
            mImageView.setImageBitmap(imageBitmap);
        }
    }

    public void UpdateText_onClick(View view) {
        EditText editText = (EditText) findViewById(R.id.txtComment);
        DrawImageView mImageView = (DrawImageView) findViewById(R.id.drawImageView2);
        mImageView.inputText = editText.getText().toString();
    }

    public void StopDrawing_onClick(View view) {
        DrawImageView mImageView = (DrawImageView) findViewById(R.id.drawImageView2);
        mImageView.drawable = false;
    }
}
