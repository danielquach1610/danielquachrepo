package com.example.danhquach.finalexam;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ListView;

import java.util.ArrayList;
import java.util.Timer;
import java.util.TimerTask;

public class MainActivity extends AppCompatActivity {
    static ArrayList tasks = new ArrayList<String>();  //Actual data
    ArrayAdapter<String> myAdapter;             //Display on UI
    Handler timerHandler = new Handler();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        // Create an array adapter
        myAdapter=new
                ArrayAdapter<String>(
                this,
                android.R.layout.simple_list_item_1,
                tasks);
        ListView myList = (ListView) findViewById(R.id.listView1);
        myList.setAdapter(myAdapter);

        timerHandler.postDelayed(timerRunnable, 0);

//        new Timer().scheduleAtFixedRate(new TimerTask() {
//            @Override
//            public void run() {
//                //Split string into (0) lst, (1) lon, (2) time
//                for (int i=0; i < tasks.size(); i++){
//                    String target = String.valueOf(tasks.get(i));
//                    String[] parts = target.split(",");
//                    String timeString = parts[2];
//
//                    String[] timeTokens = timeString.split(":");
//                    //timeToken[1] = number of seconds starting at 59
//
//                    int seconds = Integer.parseInt(timeTokens[1]);
//                    seconds -= 1;
//                    String secondsString = "00:" + seconds;
//                    if(seconds == 0) {
//                        //tasks.remove(i);
//                    }
//                    else {
//                        target = parts[0] + "," + parts[1] + "," + secondsString;
//                        tasks.add(target);
//                    }
//                    myAdapter.notifyDataSetChanged();
//                }
//            }
//        }, 0, 10000);
    }

    Runnable timerRunnable = new Runnable() {
        @Override
        public void run() {
            //Split string into (0) lst, (1) lon, (2) time
            for (int i=0; i < tasks.size(); i++){
                String target = String.valueOf(tasks.get(i));
                String[] parts = target.split(",");
                String timeString = parts[2];

                String[] timeTokens = timeString.split(":");
                //timeToken[1] = number of seconds starting at 59

                int seconds = Integer.parseInt(timeTokens[1]);
                seconds -= 1;
                String secondsString = "00:" + seconds;
                if(seconds == 0) {
                    tasks.remove(i);
                }
                else {
                    target = parts[0] + "," + parts[1] + "," + secondsString;
                    tasks.set(i,target);
                }
                myAdapter.notifyDataSetChanged();
                timerHandler.postDelayed(this, 1000);
            }
        }
    };

    public void tagTarget(View v) {
        //launch maps activity
        startActivity(new Intent(MainActivity.this, MapsActivity.class));
    }
}
