package com.example.danhquach.prog8215_assignment01;

import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.NumberPicker;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.SeekBar;
import android.widget.Spinner;
import android.widget.TextView;

import java.util.Calendar;

public class MainActivity extends AppCompatActivity {

    Spinner sprEyeColor;
    SharedPreferences prefs;                    //declare SharedPreferences
    SharedPreferences.Editor prefsEditor;       //declare SharedPreferences Editor
    @Override

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        sprEyeColor = (Spinner)findViewById(R.id.spinnerEyeColor);
        // Create an ArrayAdapter using the string array and a default spinner layout
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this,
                R.array.eyecolor_array, android.R.layout.simple_spinner_item);
        // Specify the layout to use when the list of choices appears
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        // Apply the adapter to the spinner
        sprEyeColor.setAdapter(adapter);

        //Init the objects used to save to the HDD
        prefs = getSharedPreferences("myPreferences",MODE_PRIVATE);
        prefsEditor = prefs.edit();

        //Instantiate and Set value for pickers
        NumberPicker npDay = (NumberPicker) findViewById(R.id.npDay);
        NumberPicker npMonth = (NumberPicker) findViewById(R.id.npMonth);
        NumberPicker npYear = (NumberPicker) findViewById(R.id.npYear);
        npDay.setMinValue(1);
        npDay.setMaxValue(31);
        npDay.setWrapSelectorWheel(true);
        npMonth.setMinValue(1);
        npMonth.setMaxValue(12);
        npYear.setMinValue(1900);
        int year = Calendar.getInstance().get(Calendar.YEAR);
        npYear.setMaxValue(year);

        //get value seekbar to label pant size
        SeekBar skbPantSize = (SeekBar) findViewById(R.id.skbPantSize);
        final TextView lblPantSize = (TextView) findViewById(R.id.lblPantSize);
        int seekValuePant = skbPantSize.getProgress();
        String strValuePant = "Pant Size: " + Integer.toString(seekValuePant);
        lblPantSize.setText(strValuePant);
        skbPantSize.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int i, boolean b) {
                String strValue = "Pant Size: " + Integer.toString(i);
                lblPantSize.setText(strValue);
            }
            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {}

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {}
        });

        //get value seekbar to label shoe size
        SeekBar skbShoeSize = (SeekBar) findViewById(R.id.skbShoeSize);
        final TextView lblShoeSize = (TextView) findViewById(R.id.lblShoeSize);
        int seekValueShoe = skbShoeSize.getProgress();
        String strValueShoe = "Shoe Size: " + Integer.toString(seekValueShoe);
        lblShoeSize.setText(strValueShoe);
        skbShoeSize.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int i, boolean b) {
                String strValue = "Shoe Size: " + Integer.toString(i);
                lblShoeSize.setText(strValue);
            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {}

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {}
        });

        //Read from local HDD
        TextView txtName = (TextView) findViewById(R.id.txtName);
        txtName.setText(prefs.getString("name",null));

        CheckBox chbThink = (CheckBox) findViewById(R.id.chbThink);
        chbThink.setChecked(prefs.getBoolean("chbThink",false));

        Spinner spinnerEyeColor = (Spinner) findViewById(R.id.spinnerEyeColor);
        spinnerEyeColor.setSelection(prefs.getInt("eyeColor",0));

        npDay.setValue(prefs.getInt("Day",1));
        npMonth.setValue(prefs.getInt("Month",1));
        npYear.setValue(prefs.getInt("Year",year));

        RadioGroup radgShirtSize = (RadioGroup) findViewById(R.id.radgShirtSize);
        radgShirtSize.check(prefs.getInt("shirtSize",0));

        skbPantSize.setProgress(prefs.getInt("pantSize",3));
        skbShoeSize.setProgress(prefs.getInt("shoeSize",3));
    }

    public void SaveToPrefs(View v) {
        //save Name
        TextView txtName = (TextView) findViewById(R.id.txtName);
        prefsEditor.putString("name",txtName.getText().toString());
        //save Checkbox
        CheckBox chbThink = (CheckBox) findViewById(R.id.chbThink);
        prefsEditor.putBoolean("chbThink",chbThink.isChecked());
        //save Spinner
        Spinner spinnerEyeColor = (Spinner) findViewById(R.id.spinnerEyeColor);
        prefsEditor.putInt("eyeColor",spinnerEyeColor.getSelectedItemPosition());
        //save DOB
        NumberPicker npDay = (NumberPicker) findViewById(R.id.npDay);
        NumberPicker npMonth = (NumberPicker) findViewById(R.id.npMonth);
        NumberPicker npYear = (NumberPicker) findViewById(R.id.npYear);
        prefsEditor.putInt("Day",npDay.getValue());
        prefsEditor.putInt("Month",npMonth.getValue());
        prefsEditor.putInt("Year",npYear.getValue());
        //save Shirt size
        RadioGroup radgShirtSize = (RadioGroup) findViewById(R.id.radgShirtSize);
        int selectedID = radgShirtSize.getCheckedRadioButtonId();
        prefsEditor.putInt("shirtSize",selectedID);
        //save pant size
        SeekBar seekBarPantSize = (SeekBar) findViewById(R.id.skbPantSize);
        int pantSize = seekBarPantSize.getProgress();
        prefsEditor.putInt("pantSize", pantSize);
        //save shoe size
        SeekBar seekBarShoeSize = (SeekBar) findViewById(R.id.skbShoeSize);
        int shoeSize = seekBarShoeSize.getProgress();
        prefsEditor.putInt("shoeSize", shoeSize);

        prefsEditor.commit();
    }
}
