﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Assignment02;

namespace Assignment02.Controllers
{
    public class ProductsController : Controller
    {
        private ProductDbEntities db = new ProductDbEntities();

        // GET: Products
        public ActionResult Index()
        {
            return View(db.Products.ToList());
        }

        // GET: Products/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Product product = db.Products.Find(id);
            if (product == null)
            {
                return HttpNotFound();
            }
            return View(product);
        }

        // GET: Products/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Products/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "ProductId,ProductName,Date,Quantity,Price")] Product product)
        {
            if (ModelState.IsValid)
            {
                db.Products.Add(product);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(product);
        }

        // GET: Products/Sale
        public ActionResult Sale()
        {
            return View();
        }

        // GET: Products/GetProductList
        public JsonResult GetProductList()
        {
            var query = from product in db.Products
                        join sale in db.Sales on product.ProductId equals sale.ProductId into tmpSaleP
                        from subTable in tmpSaleP.DefaultIfEmpty()
                        where product.Quantity > 0
                        select new
                        {
                            pId = product.ProductId,
                            pName = product.ProductName,
                            pQuantity = product.Quantity
                        };
            return Json(query.Distinct(), JsonRequestBehavior.AllowGet);
        }

        // POST: Products/Sale
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Sale([Bind(Include = "SaleId,ProductId,Date,Quantity,Rate")] Sale sale)
        {
            if (ModelState.IsValid)
            {
                var product = db.Products.Where(p => p.ProductId == sale.ProductId).SingleOrDefault();
                if (product != null)
                {
                    if(sale.Quantity >= 0)
                    {
                        product.Quantity += sale.Quantity;
                    }
                    else
                    {
                        if(product.Quantity < sale.Quantity)
                        {
                            product.Quantity = sale.Quantity = 0;
                        }
                        else
                        {
                            product.Quantity += sale.Quantity;
                        }
                    }
                    db.Sales.Add(sale);
                }
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(sale);
        }

        // GET: Products/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Product product = db.Products.Find(id);
            if (product == null)
            {
                return HttpNotFound();
            }
            return View(product);
        }

        // POST: Products/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "ProductId,ProductName,Date,Quantity,Price")] Product product)
        {
            if (ModelState.IsValid)
            {
                db.Entry(product).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(product);
        }

        // GET: Products/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Product product = db.Products.Find(id);
            if (product == null)
            {
                return HttpNotFound();
            }
            return View(product);
        }

        // POST: Products/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Product product = db.Products.Find(id);
            db.Products.Remove(product);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        // GET: Products/Report
        public ActionResult Report()
        {
            return View();
        }

        // GET: Products/ReportPurchase
        public JsonResult ReportPurchase()
        {
            var query = from product in db.Products
                        join sale in db.Sales on product.ProductId equals sale.ProductId into tmpSaleP
                        from subTable in tmpSaleP.DefaultIfEmpty()
                        where subTable.Quantity > 0
                        select new ReportResult
                        {
                            ProductID = product.ProductId,
                            ProductName = product.ProductName,
                            PurchaseDate = subTable.Date.Value,
                            Quantity = subTable.Quantity.Value,
                            Rate = subTable.Rate.Value
                        };
            return Json(query, JsonRequestBehavior.AllowGet);
        }

        // GET: Products/ReportSold
        public JsonResult ReportSold()
        {
            var query = from product in db.Products
                        join sale in db.Sales on product.ProductId equals sale.ProductId into tmpSaleP
                        from subTable in tmpSaleP.DefaultIfEmpty()
                        where subTable.Quantity < 0
                        select new ReportResult
                        {
                            ProductID = product.ProductId,
                            ProductName = product.ProductName,
                            PurchaseDate = subTable.Date.Value,
                            Quantity = subTable.Quantity.Value,
                            Rate = subTable.Rate.Value
                        };
            return Json(query, JsonRequestBehavior.AllowGet);
        }

        // GET: Products/ReportInventory
        public JsonResult ReportInventory()
        {
            var query = from product in db.Products
                        join sale in db.Sales on product.ProductId equals sale.ProductId into tmpSaleP
                        from subTable in tmpSaleP.DefaultIfEmpty()
                        where product.Quantity > 0
                        select new ReportResult
                        {
                            ProductID = product.ProductId,
                            ProductName = product.ProductName,
                            Quantity = product.Quantity.Value,
                            Rate = product.Price.Value
                        };
            return Json(query.Distinct(), JsonRequestBehavior.AllowGet);
        }

        // GET: Products/Benefit
        public JsonResult Benefit()
        {
            var avgPurchase = from prod in db.Products
                              join sale in db.Sales on prod.ProductId equals sale.ProductId
                              where sale.Quantity > 0
                              group sale.Rate by sale.ProductId into g
                              select new
                              {
                                  ProductId = g.Key,
                                  AverageP = g.Average()
                              };
            var avgSold = from prod in db.Products
                          join sale in db.Sales on prod.ProductId equals sale.ProductId
                          where sale.Quantity < 0
                          group sale.Rate by sale.ProductId into g
                          select new
                          {
                              ProductId = g.Key,
                              AverageS = g.Average()
                          };
            var soldQuantity = from s in db.Sales
                               where s.Quantity < 0
                               group s.Quantity by s.ProductId into g
                               select new
                               {
                                   ProductId = g.Key,
                                   soldQ = g.Sum()
                               };
            var query2 = from p in avgPurchase
                         join s in avgSold on p.ProductId equals s.ProductId
                         join sq in soldQuantity on p.ProductId equals sq.ProductId
                         join prod in db.Products on p.ProductId equals prod.ProductId
                         select new CalculateBenefit
                         {
                             ProductName = prod.ProductName,
                             AvgPurchased = p.AverageP.Value,
                             AvgSold = s.AverageS.Value,
                             SoldQuantity = sq.soldQ.Value
                         };
            return Json(query2, JsonRequestBehavior.AllowGet);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }

    class ReportResult
    {
        public int ProductID { get; set; }
        public string ProductName { get; set; }
        public DateTime PurchaseDate { get; set; }
        public string PurchaseDateStr { get { return PurchaseDate.ToShortDateString(); } set { } }
        public int Quantity { get; set; }
        public double Rate { get; set; }
    }
    class CalculateBenefit
    {
        public string ProductName { get; set; }
        public double AvgPurchased { get; set; }
        public double AvgSold { get; set; }
        public int SoldQuantity { get; set; }
        public double Benefit
        {
            get { return (AvgSold - AvgPurchased) * SoldQuantity; }
            set { }
        }
    }
}
