﻿/// <reference path="../angular.min.js" />

var app = angular.module('MarketApp', [])
app.controller('ReportController', function ($scope, $http, $window) {
    $http({
        method: "GET",
        url: "/Products/ReportSold",
        dataType: "json",
        headers: { "Content-Type": "application/json" }
    }).then(
        function (response) {
            $scope.ReportSold = response.data;
            $scope.ajax_message = "Report 1 loaded" + $scope.ReportSold;
        },
        function (response) {
            $scope.ajax_message = "Response Error: " + response.status + " : " + response.statusText;
        }
    );
    $http({
        method: "GET",
        url: "/Products/ReportPurchase",
        dataType: "json",
        headers: { "Content-Type": "application/json" }
    }).then(
        function (response) {
            $scope.ReportPurchase = response.data;
            $scope.ajax_message = "Report 2 loaded" + $scope.ReportPurchase;
        },
        function (response) {
            $scope.ajax_message = "Response Error: " + response.status + " : " + response.statusText;
        }
    );
    $http({
        method: "GET",
        url: "/Products/ReportInventory",
        dataType: "json",
        headers: { "Content-Type": "application/json" }
    }).then(
        function (response) {
            $scope.ReportInventory = response.data;
            $scope.r3msg = "Report 3 loaded" + $scope.ReportInventory;
        },
        function (response) {
            $scope.r3msg = "Response Error: " + response.status + " : " + response.statusText;
        }
    );
    $http({
        method: "GET",
        url: "/Products/Benefit",
        dataType: "json",
        headers: { "Content-Type": "application/json" }
    }).then(
        function (response) {
            $scope.Benefit = response.data;
            $scope.r4msg = "Report 4 loaded" + $scope.Benefit;
        },
        function (response) {
            $scope.r4msg = "Response Error: " + response.status + " : " + response.statusText;
        }
    );
});