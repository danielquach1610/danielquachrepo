﻿var app = angular.module('MarketApp', [])
app.controller('ProductController', function ($scope, $http, $window) {
    $http({
        method: "GET",
        url: "/Products/GetProductList",
        dataType: "json",
        headers: { "Content-Type": "application/json" }
    }).then(
        function (response) {
            console.log(response);
            $scope.ProductList = response.data;
            $scope.ajax_message = "Product List loaded" + $scope.ProductList;
        },
        function (response) {
            console.log(response);
            $scope.ajax_message = "Response Error: " + response.status + " : " + response.statusText;
        }
        );
});