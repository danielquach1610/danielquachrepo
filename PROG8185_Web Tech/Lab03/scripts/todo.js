// JavaScript source code
/// <reference path="../js/angular.min.js" />

function todoController($scope) {
    $scope.todoListA = []; //instantiate todoListA object
    $scope.todoListB = []; //instantiate todoListB object
    $scope.todoListC = []; //temporary list
    $scope.newToDoText = "";
    //return todoList items
    $scope.getToDoCount = function () {
        return $scope.todoListA.length;
    }
	
    //add new item into todoListA
    $scope.addToDoItem = function () {
        $scope.todoListA.push(
            { text: $scope.newToDoText, done: false }
        );
        $scope.newToDoText = "";
    }

    //check if empty textbox
    $scope.checkEmptyTextbox = function () {
        if ($scope.newToDoText.length > 0) {
            return false;
        }
        else {
            return true;
        }
    }

    //check empty listB
    $scope.checkEmptyList = function () {
        var isTrue = true;
        $scope.todoListB.forEach(function (element) {
            if (element.done) {
                isTrue = false;
            }
        });
        return isTrue;
    }

    //move item from listA to listB
    $scope.moveToDoItem = function (todo) {
        $scope.todoListB.push(
            { text: todo.text, done: false }
        );
        var index = $scope.todoListA.indexOf(todo);
        $scope.todoListA.splice(index, 1);
    }

    //move list of check item from listB to listA
    $scope.moveListItems = function () {
        $scope.todoListC = $scope.todoListB.filter(function (todo) {
            return todo.done;
        });
        $scope.cleanDone();
        for (i = 0; i < $scope.todoListC.length; i++) {
            $scope.todoListA.push(
                { text: $scope.todoListC[i].text, done: false }
            );
        }
    }

    //filter todoList with item which is done = true then replace old list with the new one
    $scope.cleanDone = function () {
        $scope.todoListB = $scope.todoListB.filter(function (todo) {
            return !todo.done;
        });
    }
}

var todoApp = angular.module("todoModule", []);
todoApp.controller("todoController", todoController);

//angular.module("todoModule", [])
//    .controller("todoController", function ($scope) {
//        $scope.totalToDos = 4;
//    });