$(function () {
    //add datepicker to textbox
    $("#dtpDate").datepicker();

    //custom rule for Name element, validate alphanumeric input
    jQuery.validator.addMethod("alphaNumeric", function (value, element) {
        return this.optional(element) || /^[a-zA-Z0-9]+$/.test(value);
    }, 'Please enter alphanumeric characters only');

    //prevent form to perform submit
    $("#frmMain").submit(function (event) {
        event.preventDefault();
    });

    //Set validator on
    jQuery.validator.setDefaults({
        debug: true,
        success: "valid"
    });

    //validation for input value
    $("#frmMain").validate({
        rules: {
            dtpDate: {
                date: true
            },
            txtExtension: {
                extension: "csv|xls|doc|xlsx|docx"
            },
            txtName: {
                alphaNumeric: true
            }
        }
    });
});