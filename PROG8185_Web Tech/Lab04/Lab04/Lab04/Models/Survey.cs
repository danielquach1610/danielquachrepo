﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Lab04.Models
{
    public class AnswerList
    {
        public AnswerList()
        {
        }

        public AnswerList(int iD, string answer)
        {
            ID = iD;
            Answer = answer;
        }

        public int ID { get; set; }
        public string Answer { get; set; }
    }
}