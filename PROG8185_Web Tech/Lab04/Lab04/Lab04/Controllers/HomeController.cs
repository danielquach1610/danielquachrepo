﻿using Lab04.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Lab04.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index(string message, AnswerList al)
        {
            if (message != string.Empty) ViewBag.Message = message;
            else ViewBag.Message = string.Empty;
            return View(al);
        }

        // GET: Survey
        public ActionResult Survey(int id)
        {
            ViewBag.Param = id;
            return View();
        }

        public ActionResult Cancel()
        {
            TempData["msg"] = "Last survey was cancelled";
            return RedirectToAction("Index");
        }

        [HttpPost]
        public ActionResult Survey(string[] chkSelection, string txtInput, string radioG1)
        {
            AnswerList answer = new AnswerList();
            string result = string.Empty;
            if(chkSelection != null)
            {
                foreach(var s in chkSelection)
                {
                    result += "[" + s + "]";
                }
            }
            if (txtInput != string.Empty)
            {
                result += txtInput;
            }
            if (radioG1 != string.Empty)
            {
                result += radioG1;
            }
            var url = Url.RequestContext.RouteData.Values["id"];
            answer.ID = int.Parse(url.ToString());
            answer.Answer = result;
            return View("Index", answer);
        }
    }
}